﻿using System;

namespace Arthas.Blogs.Infrastructure.Data
{
    public interface IEntity 
    {
    }

    public interface IEntity<EID>
    {
        EID ID { get; }
    }

    [Serializable]
    public abstract class EntityBase<TIdentityKey> : IEntity<TIdentityKey>,IEntity
    {
        public EntityBase()
        {
            IsDeleted = false;
        }

        public TIdentityKey ID
        {
            get;
            protected set;
        }

        public bool IsDeleted { get; set; }
    }
}
