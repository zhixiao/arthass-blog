﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Data
{
    public interface ISpecification<TEntity>
    {
        Expression<Func<TEntity, bool>> Predicate { get; }
        bool isSatisified(TEntity entity);
    }

    public class Specification<TEntity> : ISpecification<TEntity>
    {
        #region Constructors

        public Specification(Expression<Func<TEntity, bool>> expression)
        {
            Predicate = expression;
        }

        public virtual Expression<Func<TEntity, bool>> Predicate { get; private set; }

        public virtual bool isSatisified(TEntity entity)
        {
            return Predicate.Compile()(entity);
        }

        #endregion

        public static readonly Specification<TEntity> All = new Specification<TEntity>(it => true);

        public Specification<TEntity> And(ISpecification<TEntity> right)
        {
            return new Specification<TEntity>(Predicate.And(right.Predicate));
        }

        public Specification<TEntity> Or(ISpecification<TEntity> right)
        {
            return new Specification<TEntity>(Predicate.Or(right.Predicate));
        }

        public Specification<TEntity> Not()
        {
            return new Specification<TEntity>(Expression.Lambda<Func<TEntity, bool>>(Expression.Not(Predicate.Body), Predicate.Parameters.Single()));
        }
    }

    public static class SpecificationExtensions
    {
        public static ISpecification<TEntity> And<TEntity>(this ISpecification<TEntity> left, ISpecification<TEntity> right)
        {
            return new Specification<TEntity>(left.Predicate.And(right.Predicate));
        }

        public static ISpecification<TEntity> Or<TEntity>(this ISpecification<TEntity> left, ISpecification<TEntity> right)
        {
            return new Specification<TEntity>(left.Predicate.Or(right.Predicate));
        }

        public static ISpecification<TEntity> Not<TEntity>(this ISpecification<TEntity> spec) where TEntity : class
        {
            return new Specification<TEntity>(Expression.Lambda<Func<TEntity, bool>>(Expression.Not(spec.Predicate.Body), spec.Predicate.Parameters.Single()));
        }
    }

    public static class ExpresssionExtensions
    {
        public static Expression<T> Compose<T>(this Expression<T> first,Expression<T> second,Func<Expression,Expression,Expression> merge)
        {
            var map = first.Parameters.Select((f, i) => new { f, s = second.Parameters[i] }).ToDictionary(p => p.s, p => p.f);
            var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);
            return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
        }


        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.And); 
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second) 
        {
            return first.Compose(second, Expression.Or); 
        }

        class ParameterRebinder : ExpressionVisitor
        {
            readonly Dictionary<ParameterExpression, ParameterExpression> map;

            ParameterRebinder(Dictionary<ParameterExpression, ParameterExpression> map) 
            {
                this.map = map ?? new Dictionary<ParameterExpression, ParameterExpression>(); 
            }

            public static Expression ReplaceParameters(Dictionary<ParameterExpression, ParameterExpression> map, Expression exp)
            {
                return new ParameterRebinder(map).Visit(exp); 
            }

            protected override Expression VisitParameter(ParameterExpression p)
            {
                ParameterExpression replacement;

                if (map.TryGetValue(p, out replacement))
                    p = replacement;

                return base.VisitParameter(p);
            }
        }
    }
}
