﻿using System;

namespace Arthas.Blogs.Infrastructure.Data.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}
