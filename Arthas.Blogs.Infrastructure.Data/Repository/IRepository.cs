﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Data.Repository
{
    public interface IRepository<TEntity, in TKey>
        where TEntity : EntityBase<TKey>
    {
        #region Create

        TSubEntity Add<TSubEntity>(TSubEntity entity) where TSubEntity : TEntity;

        #endregion

        #region Query

        TEntity GetById(TKey id);

        IQueryable<TEntity> Query(
            Expression<Func<TEntity, bool>> whereClause = null,
            int pageIndex = 0,
            int pageSize = 50);

        IQueryable<TEntity> Query(
            out int totalCount,
            Expression<Func<TEntity, bool>> whereClause = null,
            int pageIndex = 0,
            int pageSize = 50);

        IQueryable<TEntity> Query<TOrderByKey>(
            Expression<Func<TEntity, bool>> whereClause = null,
            Expression<Func<TEntity, TOrderByKey>> orderbyClause = null,
            bool isDescending = false,
            int pageIndex = 0,
            int pageSize = 50);

        IQueryable<TEntity> Query<TOrderByKey>(
            out int totalCount,
            Expression<Func<TEntity, bool>> whereClause = null,
            Expression<Func<TEntity, TOrderByKey>> orderbyClause = null,
            bool isDescending = false,
            int pageIndex = 0,
            int pageSize = 50);

        #endregion

        #region Update

        void Update<TSubEntity>(TSubEntity entity) where TSubEntity : TEntity;

        #endregion

        #region Remove

        void Remove<TSubEntity>(TSubEntity entity) where TSubEntity : TEntity;

        #endregion
    }
}
