﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Arthas.Blogs.Infrastructure.Data.Repository
{
    public class Repository<TEntity,TKey> : IRepository<TEntity,TKey>
        where TEntity : EntityBase<TKey>
    {
        public Repository(DbContext dbContext)
        {
            if (dbContext == null) throw new ArgumentNullException("dbContext");
            DbContext = dbContext;
        }

        protected virtual DbContext DbContext
        {
            get;
            private set;
        }

        #region Create

        public virtual TSubEntity Add<TSubEntity>(TSubEntity entity) where TSubEntity : TEntity
        {
            if (entity == null) throw new ArgumentNullException("entity");
            return DbContext.Set<TSubEntity>().Add(entity);
        }

        #endregion

        #region Query

        public virtual TEntity GetById(TKey id)
        {
            return DbContext.Set<TEntity>().Find(id);
        }

        public virtual IQueryable<TEntity> Query(
            Expression<Func<TEntity, bool>> whereClause = null,
            int pageIndex = 0,
            int pageSize = 50)
        {
            return Query<TKey>(
               whereClause,
               entity => entity.ID,
               false,
               pageIndex,
               pageSize);
        }

        public virtual IQueryable<TEntity> Query(
            out int totalCount,
            Expression<Func<TEntity, bool>> whereClause = null,
            int pageIndex = 0,
            int pageSize = 50)
        {
            return Query<TKey>(
                out totalCount,
                whereClause,
                entity => entity.ID,
                false,
                pageIndex,
                pageSize);
        }

        public virtual IQueryable<TEntity> Query<TOrderByKey>(
            Expression<Func<TEntity, bool>> whereClause = null,
            Expression<Func<TEntity, TOrderByKey>> orderbyClause = null,
            bool isDescending = false,
            int pageIndex = 0,
            int pageSize = 50)
        {
            int totalCount = 0;
            return Query(out totalCount, whereClause, orderbyClause, isDescending, pageIndex, pageSize);
        }

        public virtual IQueryable<TEntity> Query<TOrderByKey>(
            out int totalCount,
            Expression<Func<TEntity, bool>> whereClause = null,
            Expression<Func<TEntity, TOrderByKey>> orderbyClause = null,
            bool isDescending = false,
            int pageIndex = 0,
            int pageSize = 50)
        {
            var query = DbContext.Set<TEntity>().AsQueryable();

            if (whereClause != null)
            {
                query = query.Where(whereClause);
            }

            if (orderbyClause != null)
            {
                if (isDescending)
                {
                    query = query.OrderByDescending(orderbyClause);
                }
                else
                {
                    query = query.OrderBy(orderbyClause);
                }
            }
            else
            {
                query = query.OrderBy(entity => entity.ID);
            }

            totalCount = query.Count();

            query = query.Skip(pageIndex * pageSize).Take(pageSize);

            return query;
        }

        #endregion

        #region Update

        public virtual void Update<TSubEntity>(TSubEntity entity) where TSubEntity : TEntity
        {
            DbContext.Set<TSubEntity>().Attach(entity);
            DbContext.Entry<TSubEntity>(entity).State = EntityState.Modified;
        }

        #endregion

        #region Remove

        public virtual void Remove<TSubEntity>(TSubEntity entity) where TSubEntity : TEntity
        {
            if (entity == null)
            {
                throw new ArgumentException("entity");
            }
            DbContext.Set<TSubEntity>().Attach(entity);
            DbContext.Set<TSubEntity>().Remove(entity);
        }

        #endregion
    }
}
