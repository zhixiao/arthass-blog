﻿using System.Data.Entity;

namespace Arthas.Blogs.Infrastructure.Data.Repository.EF
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;

        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        protected DbContext DbContext
        {
            get
            {
                return _dbContext;
            }
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }
    }
}
