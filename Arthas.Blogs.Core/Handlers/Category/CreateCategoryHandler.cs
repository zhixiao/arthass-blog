﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class CategoryHandler : HandlerBase
    {
        public void Handle(CreateCategoryCommand command)
        {
            var toCreate = new Category();
            toCreate.CategoryName = command.CategoryName;

            DataContext.CategoryRepository.Add(toCreate);

            UOW.CommitFor(command);
        }
    }
}
