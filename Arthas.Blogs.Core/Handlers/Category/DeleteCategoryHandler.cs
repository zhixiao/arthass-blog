﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;


namespace Arthas.Blogs.Core.Handlers
{
    public partial class CategoryHandler : HandlerBase
    {
        public void Handle(DeleteCategoryCommand command)
        {
            var toDelete = DataContext.CategoryRepository.GetById(command.CategoryID);
            if (toDelete == null)
            {
                return;
            }

            toDelete.IsDeleted = true;
            DataContext.CategoryRepository.Update(toDelete);

            UOW.CommitFor(command);
        }
    }
}
