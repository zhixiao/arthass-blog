﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class CategoryHandler : HandlerBase
    {
        public void Handle(EditCategoryCommand command)
        {
            var toEdit = DataContext.CategoryRepository.GetById(command.CategoryID);
            if (toEdit == null)
            {
                return;
            }
            toEdit.CategoryName = command.CategoryName;

            DataContext.CategoryRepository.Update(toEdit);

            UOW.CommitFor(command);
        }
    }
}
