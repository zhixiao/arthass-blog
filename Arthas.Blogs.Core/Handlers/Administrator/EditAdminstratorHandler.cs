﻿
using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class AdministratorHandler
    {
        public void Handle(EditAdministratorCommand command)
        {
            var toEdit = DataContext.AdministratorRepository.GetById(command.ID);

            if (string.IsNullOrEmpty(command.Password) == false)
            {
                toEdit.PassWord = Administrator.HashPassword(command.Password);
            }

            toEdit.IsActived = command.IsActived;
            toEdit.AboutMe = command.AboutMe;
            toEdit.ProfileImage = command.ProfileImage;
            DataContext.AdministratorRepository.Update(toEdit);

            UOW.CommitFor(command);
        }
    }
}
