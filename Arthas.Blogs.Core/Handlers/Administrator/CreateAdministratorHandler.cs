﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;
using System;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class AdministratorHandler : HandlerBase
    {
        public void Handle(CreateAdministratorCommand command)
        {
            var hashPassword = Administrator.HashPassword(command.Password);
            var toCreate = new Administrator(command.UserName, hashPassword);
            toCreate.CreateDate = DateTime.UtcNow;
            toCreate.IsActived = command.IsActived;
            toCreate.AboutMe = command.AboutMe;
            toCreate.ProfileImage = command.ProfileImage;
            toCreate.IsDeleted = false;

            DataContext.AdministratorRepository.Add(toCreate);

            UOW.CommitFor(command);
        }
    }
}
