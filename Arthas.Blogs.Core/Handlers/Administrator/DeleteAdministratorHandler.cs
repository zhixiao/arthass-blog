﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;
namespace Arthas.Blogs.Core.Handlers
{
    public partial class AdministratorHandler : HandlerBase
    {
        public void Handle(DeleteAdministratorCommand command)
        {
            var toDelete = DataContext.AdministratorRepository.GetById(command.UserID);

            toDelete.IsDeleted = true;

            DataContext.AdministratorRepository.Update(toDelete);

            UOW.CommitFor(command);
        }
    }
}
