﻿using Arthas.Blogs.Core.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class CommentHandler : HandlerBase
    {
        public void Handle(EditCommentCommand command)
        {
            var toEdit = DataContext.CommentRepository.GetById(command.CommentID);

            toEdit.Content = command.Content;
            toEdit.CreateDateTime = DateTime.UtcNow;

            DataContext.CommentRepository.Update(toEdit);

            UOW.CommitFor(command);
        }
    }
}
