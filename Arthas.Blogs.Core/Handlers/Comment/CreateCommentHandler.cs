﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class CommentHandler : HandlerBase
    {
        public void Handle(CreateCommentCommand command)
        {
            var toCreate = new Review();
            var blog = DataContext.BlogRepository.GetById(command.BlogID);
            var reviewer = DataContext.UserRepository.GetById(command.UserID);
            if (blog == null || reviewer == null)
            {
                return;
            }

            if (command.ParentID > 0)
            {
                var parent = DataContext.CommentRepository.GetById(command.ParentID);
                if(parent != null)
                {
                    toCreate.Parent = parent;
                    parent.ReplyCount++;
                    
                }
            }
            
            toCreate.Blog = blog;
            toCreate.Reviewer = reviewer;
            toCreate.Content = command.Content;
            toCreate.CreateDateTime = DateTime.UtcNow;
            toCreate.IsDeleted = false;
            toCreate.ReplyCount = 0;

            DataContext.CommentRepository.Add(toCreate);

            UOW.CommitFor(command);
        }
    }
}
