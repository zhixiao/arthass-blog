﻿using Arthas.Blogs.Core.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class CommentHandler : HandlerBase
    {
        public void Handle(DeleteCommentCommand command)
        {
            var toDelete = DataContext.CommentRepository.GetById(command.CommentID);

            if (toDelete == null)
            {
                return;
            }

            toDelete.IsDeleted = true;

            DataContext.CommentRepository.Update(toDelete);

            UOW.CommitFor(command);
        }
    }
}
