﻿
using Arthas.Blogs.Core.Commands;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class BlogHandler : HandlerBase
    {
        public void Handle(DeleteBlogCommand command)
        {
            var toDelete = DataContext.BlogRepository.GetById(command.ID);

            if (toDelete == null)
            {
                return;
            }

            toDelete.IsDeleted = true;

            DataContext.BlogRepository.Update(toDelete);

            UOW.CommitFor(command);
        }
    }
}
