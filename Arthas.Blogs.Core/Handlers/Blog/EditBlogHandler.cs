﻿using Arthas.Blogs.Core.Commands;
using System;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class BlogHandler : HandlerBase
    {
        public void Handle(EditBlogCommand command)
        {
            var toUpdate = DataContext.BlogRepository.GetById(command.BlogID);
            if (toUpdate == null)
            {
                return;
            }

            toUpdate.Title = command.Title;
            toUpdate.Description = command.Description;
            toUpdate.Content = command.Content;
            toUpdate.LastUpdate = DateTime.UtcNow;
            toUpdate.Tags = command.Tags;
            toUpdate.Status = command.Status;

            var category = DataContext.CategoryRepository.GetById(command.CategoryID);
            if (category != null)
            {
                toUpdate.Category = category;
            }
            else
            {
                toUpdate.Category = DataContext.CategoryRepository.GetById(1);
            }

            DataContext.BlogRepository.Update(toUpdate);

            UOW.CommitFor(command);
        }
    }
}
