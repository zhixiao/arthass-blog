﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;
using System;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class BlogHandler : HandlerBase
    {
        public void Handle(CreateBlogCommand command)
        {
            var toCreate = new Blog();

            toCreate.Title = command.Title;
            toCreate.Description = command.Description;
            toCreate.Content = command.Content;
            toCreate.Tags = command.Tags;
            toCreate.Status = command.Status;

            var category = DataContext.CategoryRepository.GetById(command.CategoryID);
            if (category != null)
            {
                toCreate.Category = category;
            }
            else
            {
                toCreate.Category = DataContext.CategoryRepository.GetById(1);
            }

            toCreate.DateCategory = DateTime.UtcNow.ToString("yyyy年MM月");
            toCreate.CreateDate = DateTime.UtcNow;
            toCreate.LastUpdate = DateTime.UtcNow;
            toCreate.ApprovalCount = 0;
            toCreate.ReadCount = 0;

            DataContext.BlogRepository.Add(toCreate);

            UOW.CommitFor(command);
        }
    }
}
