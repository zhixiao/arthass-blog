﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class UserHandler : HandlerBase
    {
        public void Handle(EditUserCommand command)
        {
            var toEdit = DataContext.UserRepository.GetById(command.UserID);
            if (toEdit == null)
            {
                return;
            }

            toEdit.Password = Administrator.HashPassword(command.Password);
            toEdit.ProfileImage = command.ProfileImage;

            DataContext.UserRepository.Update(toEdit);

            UOW.CommitFor(command);
        }
    }
}
