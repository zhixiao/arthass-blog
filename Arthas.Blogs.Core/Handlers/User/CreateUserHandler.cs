﻿using System;
using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class UserHandler : HandlerBase
    {
        public void Handle(CreateUserCommand command)
        {
            var toCreate = command.ToCreate;
            if (toCreate == null)
            {
                return;
            }

            DataContext.UserRepository.Add(toCreate);

            UOW.CommitFor(command);
        }
    }
}
