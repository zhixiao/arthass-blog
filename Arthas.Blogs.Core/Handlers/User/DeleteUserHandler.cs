﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class UserHandler : HandlerBase
    {
        public void Handle(DeleteUserCommand command)
        {
            var toDelete = DataContext.UserRepository.GetById(command.UserID);
            if (toDelete == null)
            {
                return;
            }

            toDelete.IsDeleted = true;
            DataContext.UserRepository.Update(toDelete);

            UOW.CommitFor(command);
        }
    }
}
