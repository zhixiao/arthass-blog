﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;
using System;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class PhotoHandler : HandlerBase
    {
        public void Handle(DeletePhotoCommand command)
        {
            var toDelete = DataContext.PhotoRepository.GetById(command.PhotoID);
            if (toDelete == null)
            {
                return;
            }
            toDelete.IsDeleted = true;
            DataContext.PhotoRepository.Update(toDelete);

            UOW.CommitFor(command);
        }
    }
}
