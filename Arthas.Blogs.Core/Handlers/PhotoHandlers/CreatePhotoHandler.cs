﻿using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;
using System;

namespace Arthas.Blogs.Core.Handlers
{
    public partial class PhotoHandler : HandlerBase
    {
        public void Handle(CreatePhotoCommand command)
        {
            var toCreate = new Photo();
            toCreate.PhotoName = command.PhotoName;
            toCreate.PhotoPath = command.PhotoPath;
            toCreate.Usefulness = command.Usefulness;
            toCreate.CreationDate = DateTime.UtcNow;
            toCreate.IsDeleted = false;

            DataContext.PhotoRepository.Add(toCreate);

            UOW.CommitFor(command);
        }
    }
}
