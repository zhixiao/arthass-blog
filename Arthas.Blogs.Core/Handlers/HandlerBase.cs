﻿using Arthas.Blogs.Infrastructure.Tool.Ioc;
using Arthas.Blogs.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Handlers
{
    public abstract class HandlerBase
    {
        protected IWebshopUnitOfWork UOW
        {
            get
            {
                return DataContext;
            }
        }

        private IDataContext _DataContext;
        protected IDataContext DataContext
        {
            get
            {
                if (_DataContext == null)
                {
                    _DataContext = Container.Current.Resolve<IDataContext>();
                }
                return _DataContext;
            }
        }

        private WebshopBus _Bus;
        protected WebshopBus Bus
        {
            get
            {
                if (_Bus == null)
                {
                    _Bus = Container.Current.Resolve<IWebshopBus>() as WebshopBus;
                }

                return _Bus;
            }
            set
            {
                _Bus = value;
            }
        }
    }
}
