﻿using Arthas.Blogs.Infrastructure.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core
{
    public interface IWebshopUnitOfWork : IUnitOfWork
    {
        event Action OnCommitted;
    }
}
