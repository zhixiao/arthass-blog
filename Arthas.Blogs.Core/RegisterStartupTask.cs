﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core
{
    using Arthas.Blogs.Infrastructure.Tool;
    using Arthas.Blogs.Infrastructure.Tool.Ioc;
    using Arthas.Blogs.Core.Handlers;
    using Arthas.Blogs.Core.Queries;

    public class RegisterStartupTask : IStartupTask
    {
        public void Execute()
        {

            #region CQRS

            Container.Current.Register<IWebshopBus, WebshopBus>();
            Container.Current.Register<AdministratorHandler>();
            Container.Current.Register<CategoryHandler>();
            Container.Current.Register<BlogHandler>();
            Container.Current.Register<PhotoHandler>();
            Container.Current.Register<CommentHandler>();
            Container.Current.Register<UserHandler>();

            #endregion
        }

        public int Order
        {
            get { return 0; }
        }
    }
}
