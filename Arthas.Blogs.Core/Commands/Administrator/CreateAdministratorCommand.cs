﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Commands
{
    public class CreateAdministratorCommand : CommandBase
    {
        public CreateAdministratorCommand(string userName, string password, bool isActived, string aboutMe, string profileImage)
        {
            UserName = userName;
            Password = password;
            IsActived = isActived;
            AboutMe = aboutMe;
            ProfileImage = profileImage;
        }

        public string UserName { get; set; }

        public string Password { get; set; }

        public bool IsActived { get; set; }

        public string AboutMe { get; protected set; }

        public string ProfileImage { get; protected set; }
    }
}
