﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Commands
{
    public class EditAdministratorCommand : CommandBase
    {
        public EditAdministratorCommand(int id,string password,bool isactived,string aboutMe,string profileImage)
        {
            ID = id;
            Password = password;
            IsActived = isactived;
            AboutMe = aboutMe;
            ProfileImage = profileImage;
        }

        public int ID { get; protected set; }

        public string Password { get; protected set; }

        public bool IsActived { get; protected set; }

        public string AboutMe { get; protected set; }

        public string ProfileImage { get; protected set; }
    }
}
