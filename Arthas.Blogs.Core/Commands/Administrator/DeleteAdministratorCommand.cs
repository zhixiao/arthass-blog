﻿
namespace Arthas.Blogs.Core.Commands
{
    public class DeleteAdministratorCommand : CommandBase
    {
        public DeleteAdministratorCommand(int id)
        {
            UserID = id;
        }

        public int UserID { get; set; }
    }
}
