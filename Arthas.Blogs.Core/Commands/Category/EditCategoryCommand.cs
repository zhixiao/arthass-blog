﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Commands
{
    public class EditCategoryCommand : CommandBase
    {
        public EditCategoryCommand(int categoryID,string categoryName)
        {
            CategoryID = categoryID;
            CategoryName = categoryName;
        }

        public int CategoryID { get; set; }

        public string CategoryName { get; set; }
    }
}
