﻿
namespace Arthas.Blogs.Core.Commands
{
    public class CreateCategoryCommand : CommandBase
    {
        public CreateCategoryCommand(string categoryName)
        {
            CategoryName = categoryName;
        }

        public string CategoryName { get; set; }
    }
}
