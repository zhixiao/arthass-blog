﻿using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Commands
{
    public class CreatePhotoCommand : CommandBase
    {
        public CreatePhotoCommand(string name, string path, ImageStatus usefulness)
        {
            PhotoName = name;
            PhotoPath = path;
            Usefulness = usefulness;
        }

        public string PhotoName { get; set; }

        public string PhotoPath { get; set; }

        public ImageStatus Usefulness { get; set; }
    }
}
