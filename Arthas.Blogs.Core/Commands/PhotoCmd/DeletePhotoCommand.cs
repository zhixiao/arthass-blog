﻿

namespace Arthas.Blogs.Core.Commands
{
    public class DeletePhotoCommand : CommandBase
    {
        public DeletePhotoCommand(int id)
        {
            PhotoID = id;
        }

        public int PhotoID { get; set; }
    }
}
