﻿using Arthas.Blogs.Infrastructure.Tool.CQRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Commands
{
    public abstract class CommandBase : Command
    {
        internal Command Sender { get; private set; }

        internal void Send<T>(WebshopBus Bus, T subCommand) where T : CommandBase
        {
            subCommand.Sender = this;
            Bus.Send(subCommand);
        }
    }
}
