﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Commands
{
    public class ApproveBlogCommand : CommandBase
    {
        public ApproveBlogCommand(int blogID)
        {
            BlogID = blogID;
        }

        public int BlogID { get; set; }
    }
}
