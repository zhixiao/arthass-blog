﻿namespace Arthas.Blogs.Core.Commands
{
    public class DeleteBlogCommand : CommandBase
    {
        public DeleteBlogCommand(int id)
        {
            ID = id;  
        }

        public int ID { get; set; }
    }
}
