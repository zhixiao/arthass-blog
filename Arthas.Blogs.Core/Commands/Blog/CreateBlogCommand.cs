﻿
using Arthas.Blogs.Core.Model;
namespace Arthas.Blogs.Core.Commands
{
    public class CreateBlogCommand : BlogCommandBase
    {
        public CreateBlogCommand(string title, string desc, string content, int categoryID, string tags, BlogStatus status = BlogStatus.Draft)
            : base(title, desc, content, categoryID, tags, status)
        {

        }
    }
}
