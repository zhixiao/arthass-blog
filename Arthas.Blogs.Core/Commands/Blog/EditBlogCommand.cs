﻿

using Arthas.Blogs.Core.Model;
namespace Arthas.Blogs.Core.Commands
{
    public class EditBlogCommand : BlogCommandBase
    {
        public EditBlogCommand(int id,string title, string desc, string content, int categoryID, string tags, BlogStatus status = BlogStatus.Draft)
            : base(title, desc, content, categoryID, tags, status)
        {
            BlogID = id;
        }

        public int BlogID { get; protected set; }
    }
}
