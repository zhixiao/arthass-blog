﻿using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Commands
{
    public class BlogCommandBase : CommandBase
    {
        public BlogCommandBase(string title, string desc, string content, int categoryID, string tags, BlogStatus status = BlogStatus.Draft)
        {
            Title = title;
            Description = desc;
            Content = content;
            CategoryID = categoryID;
            Tags = tags;
            Status = status;
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Content { get; set; }

        public int CategoryID { get; set; }

        public string Tags { get; set; }

        public BlogStatus Status { get; set; }
    }
}
