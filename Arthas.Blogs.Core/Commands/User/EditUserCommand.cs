﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Commands
{
    public class EditUserCommand : CommandBase
    {
        public EditUserCommand(int userID,string password,string profileImage)
        {
            UserID = userID;
            Password = password;
            ProfileImage = profileImage;
        }

        public int UserID { get; protected set; }

        public string Password { get; protected set; }

        public string ProfileImage { get; protected set; }
    }
}
