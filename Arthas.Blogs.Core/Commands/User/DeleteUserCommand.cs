﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Commands
{
    public class DeleteUserCommand : CommandBase
    {
        public DeleteUserCommand(int userID)
        {
            UserID = userID;
        }

        public int UserID { get; protected set; }
    }
}
