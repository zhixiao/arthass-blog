﻿using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Commands
{
    public class CreateUserCommand : CommandBase
    {
        public CreateUserCommand(User user)
        {
            ToCreate = user;
        }

        public User ToCreate { get; protected set; }
    }
}
