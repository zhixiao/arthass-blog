﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Commands
{
    public class CreateCommentCommand : CommandBase
    {
        public CreateCommentCommand(int userID,string content,int parentID,int blogID)
        {
            UserID = userID;
            Content = content;
            ParentID = parentID;
            BlogID = blogID;
        }

        public int UserID { get; set; }

        public int ParentID { get; set; }

        public string Content { get; set; }

        public int BlogID { get; set; }
    }
}
