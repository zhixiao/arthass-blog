﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Commands
{
    public class EditCommentCommand : CommandBase
    {
        public EditCommentCommand(int commentID,string content)
        {
            CommentID = commentID;
            Content = content;
        }

        public int CommentID { get; set; }

        public string Content { get; set; }
    }
}
