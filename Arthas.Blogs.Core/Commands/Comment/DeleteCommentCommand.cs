﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Commands
{
    public class DeleteCommentCommand : CommandBase
    {
        public DeleteCommentCommand(int commentID)
        {
            CommentID = commentID;
        }

        public int CommentID { get; set; }
    }
}
