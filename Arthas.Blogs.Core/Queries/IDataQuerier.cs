﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Arthas.Blogs.Infrastructure.Data;

namespace Arthas.Blogs.Core.Queries
{
    public interface IDataQuerier
    {
        IQueryable<T> GetQuery<T>(ISpecification<T> specification) where T : class, IEntity;

        T GetOne<T>(ISpecification<T> specification = null) where T : class, IEntity;

        T GetByID<T, K>(K id) where T : class, IEntity;

        void Refresh<T>(T entry) where T : class, IEntity;
    }
}
