﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Arthas.Blogs.Infrastructure.Data;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Queries.Specifications
{
    public class ReviewSpec : SpecBase
    {
        public static Specification<Review> HasBlogID(int blogID)
        {
            return new Specification<Review>(it => it.Blog.ID == blogID);
        }

        public static Specification<Review> IsRootComment()
        {
            return new Specification<Review>(it => it.Parent == null);
        }

        public static Specification<Review> HasParentID(int parentID)
        {
            return new Specification<Review>(it => it.Parent != null && it.Parent.ID == parentID);
        }
    }
}
