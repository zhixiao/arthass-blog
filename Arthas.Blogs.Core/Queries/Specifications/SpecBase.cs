﻿using Arthas.Blogs.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Queries.Specifications
{
    public class SpecBase
    {
        public static Specification<T> IsNotDelete<T>() where T : EntityBase<int>
        {
            return new Specification<T>(it => it.IsDeleted == false);
        }
    }
}
