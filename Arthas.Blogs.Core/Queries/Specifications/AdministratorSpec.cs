﻿using Arthas.Blogs.Infrastructure.Data;
using Arthas.Blogs.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Queries.Specifications
{
    public class AdministratorSpec
    {
        public static Specification<Administrator> IsNotDeleted()
        {
            return new Specification<Administrator>(it => it.IsDeleted == false);
        }

        public static Specification<Administrator> HasKeyword(string keyword)
        {
            return new Specification<Administrator>(it => it.UserName.Contains(keyword));
        }

        public static Specification<Administrator> HasID(int id)
        {
            return new Specification<Administrator>(it => it.ID == id);
        }

        public static Specification<Administrator> HasUserName(string userName)
        {
            return new Specification<Administrator>(it => it.UserName == userName);
        }
    }
}
