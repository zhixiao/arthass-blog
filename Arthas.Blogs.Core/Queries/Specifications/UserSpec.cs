﻿using Arthas.Blogs.Infrastructure.Data;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Queries.Specifications
{
    public class UserSpec : SpecBase
    {
        public static Specification<User> HasKeyword(string keyword)
        {
            return new Specification<User>(it => it.UserName.Contains(keyword) || it.Email.Contains(keyword));
        }

        public static Specification<User> IsUserName(string userName)
        {
            return new Specification<User>(it => it.UserName == userName && it.IsDeleted == false);
        }

        public static Specification<User> IsEmail(string email)
        {
            return new Specification<User>(it => it.Email == email && it.IsDeleted == false);
        }
    }
}
