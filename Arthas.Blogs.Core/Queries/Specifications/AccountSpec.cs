﻿using Arthas.Blogs.Infrastructure.Data;
using Arthas.Blogs.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Queries.Specifications
{
    public class AccountSpec
    {
        public static Specification<Administrator> HasUserName(string userName)
        {
            return new Specification<Administrator>(it => it.UserName == userName);
        }
    }
}
