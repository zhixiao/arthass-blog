﻿using Arthas.Blogs.Infrastructure.Data;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Queries.Specifications
{
    public class PhotoSpec : SpecBase
    {
        public static Specification<Photo> IsUsefulness(ImageStatus usefulness)
        {
            return new Specification<Photo>(it => it.Usefulness == usefulness);
        }
    }
}
