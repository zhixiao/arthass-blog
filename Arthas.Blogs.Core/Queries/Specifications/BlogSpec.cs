﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arthas.Blogs.Infrastructure.Data;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Queries.Specifications
{
    public class BlogSpec
    {
        public static Specification<Blog> IsNotDelete()
        {
            return new Specification<Blog>(it => it.IsDeleted == false);
        }

        public static Specification<Blog> HasKeyword(string keyword)
        {
            return new Specification<Blog>(it => it.Title.Contains(keyword) || it.Tags.Contains(keyword));
        }

        public static Specification<Blog> IsCategory(int categoryID)
        {
            return new Specification<Blog>(it => it.Category.ID == categoryID);
        }

        public static Specification<Blog> HasBlogID(int id)
        {
            return new Specification<Blog>(it => it.ID == id);
        }
    }
}
