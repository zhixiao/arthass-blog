﻿using Arthas.Blogs.Infrastructure.Data.Repository;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Repositories
{
    public interface IPhotoRepository : IRepository<Photo,int>
    {
    }
}
