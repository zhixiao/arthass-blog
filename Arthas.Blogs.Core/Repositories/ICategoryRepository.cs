﻿using Arthas.Blogs.Infrastructure.Data.Repository;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Repositories
{
    public interface ICategoryRepository : IRepository<Category,int>
    {
    }
}
