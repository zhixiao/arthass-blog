﻿using Arthas.Blogs.Infrastructure.Data.Repository;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Repositories
{
    public interface IAdministratorRepository : IRepository<Administrator,int>
    {
    }
}
