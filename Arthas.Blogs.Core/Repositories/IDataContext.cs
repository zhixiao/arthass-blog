﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Repositories
{
    public interface IDataContext : IWebshopUnitOfWork
    {
        IAdministratorRepository AdministratorRepository { get; }

        ICategoryRepository CategoryRepository { get; }

        IBlogRepository BlogRepository { get; }

        IPhotoRepository PhotoRepository { get; }

        IUserRepository UserRepository { get; }

        ICommentRepository CommentRepository { get; }
    }
}
