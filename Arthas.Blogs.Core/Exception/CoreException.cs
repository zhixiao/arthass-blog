﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Arthas.Blogs.Infrastructure.Tool.Exceptions;
using Arthas.Blogs.Infrastructure.Tool;

namespace Arthas.Blogs.Core.Exception
{
    public class CoreException : KoobooProjectExceptionBase
    {
        public CoreException(CoreExceptionCode code)
            : base(Convert.ToInt32(code).ToString(), code.Description())
        {
            ExceptionCode = code;
        }

        public CoreException(CoreExceptionCode code, string message)
            : base(Convert.ToInt32(code).ToString(), message)
        {
            ExceptionCode = code;
        }

        public new CoreExceptionCode ExceptionCode
        {
            get;
            private set;
        }
    }
}
