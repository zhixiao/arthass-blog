﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Arthas.Blogs.Core
{
    using Arthas.Blogs.Infrastructure.Tool.CQRS;
    using Arthas.Blogs.Core.Commands;
    using Arthas.Blogs.Core.Handlers;
    

    public interface IWebshopBus
    {
        void Send<T>(T command) where T : Command;
        CommandResult GetResult(Command command);
        CommandResult GetResult(Guid commandID);
        void RegisterEventHandler<T>(Action<T> handler) where T : IDomainEvent;
    }

    public class WebshopBus : IWebshopBus
    {
        readonly ServiceBus _InnerBus;
        public WebshopBus()
        {
            _InnerBus = new ServiceBus();

            ConfigAdministratorCommand();
            ConfigCategoryCommand();
            ConfigBlogCommand();
            ConfigPhotoCommand();
            ConfigCommentCommand();
            ConfigUserCommand();
        }

        private void ConfigAdministratorCommand()
        {
            var handler = Resolve<AdministratorHandler>();
            _InnerBus.RegisterCommandHandler<CreateAdministratorCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<EditAdministratorCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<DeleteAdministratorCommand>(handler.Handle);
        }

        private void ConfigCategoryCommand()
        {
            var handler = Resolve<CategoryHandler>();
            _InnerBus.RegisterCommandHandler<CreateCategoryCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<EditCategoryCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<DeleteCategoryCommand>(handler.Handle);
        }

        private void ConfigBlogCommand()
        {
            var handler = Resolve<BlogHandler>();
            _InnerBus.RegisterCommandHandler<CreateBlogCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<EditBlogCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<DeleteBlogCommand>(handler.Handle);
        }

        private void ConfigPhotoCommand()
        {
            var handler = Resolve<PhotoHandler>();
            _InnerBus.RegisterCommandHandler<CreatePhotoCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<DeletePhotoCommand>(handler.Handle);
        }

        private void ConfigCommentCommand()
        {
            var handler = Resolve<CommentHandler>();
            _InnerBus.RegisterCommandHandler<CreateCommentCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<EditCommentCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<DeleteCommentCommand>(handler.Handle);
        }

        private void ConfigUserCommand()
        {
            var handler = Resolve<UserHandler>();
            _InnerBus.RegisterCommandHandler<CreateUserCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<EditUserCommand>(handler.Handle);
            _InnerBus.RegisterCommandHandler<DeleteUserCommand>(handler.Handle);
        }

        private T Resolve<T>() where T : class
        {
            return Arthas.Blogs.Infrastructure.Tool.Ioc.Container.Current.Resolve<T>();
        }

        #region Command

        public void Send<T>(T command) where T : Command
        {
            _InnerBus.Send(command);
        }

        private List<Command> SubCommands = new List<Command>();
        private object SubCommandsLocker = new object();
        internal void SendSub<T>(T command) where T : Command
        {
            lock (SubCommandsLocker)
            {
                SubCommands.Add(command);
            }
            _InnerBus.Send(command);
        }

        internal bool IsSubCommand(Command command)
        {
            lock (SubCommandsLocker)
            {
                return SubCommands.Any(item => item.ID == command.ID);
            }
        }

        public CommandResult GetResult(Command command)
        {
            return GetResult(command.ID);
        }

        public CommandResult GetResult(Guid commandID)
        {
            return _InnerBus.GetResult(commandID);
        }

        internal void Return(CommandResult result)
        {
            _InnerBus.Return(result);
        }

        #endregion

        #region Event

        public void RegisterEventHandler<T>(Action<T> handler) where T : IDomainEvent
        {
            _InnerBus.RegisterEventHandler<T>(handler);
        }

        internal void Publish<T>(T @event) where T : IDomainEvent
        {
            _InnerBus.Publish<T>(@event);
        }

        internal void PublishSync<T>(T @event) where T : IDomainEvent
        {
            _InnerBus.PublishSync<T>(@event);
        }

        #endregion
    }
}
