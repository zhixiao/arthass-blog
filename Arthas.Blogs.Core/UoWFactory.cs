﻿using Arthas.Blogs.Core.Commands;
namespace Arthas.Blogs.Core
{
    static class IWebshopUnitofWorkExtension
    {
        public static void CommitFor(this IWebshopUnitOfWork UOW,CommandBase command)
        {
            if (command.Sender == null)
            {
                UOW.Commit();
            }
        }
    }
}
