﻿using Arthas.Blogs.Infrastructure.Data;
using System;

namespace Arthas.Blogs.Core.Model
{
    public class Administrator : EntityBase<int>
    {
        protected Administrator() { }

        public Administrator(string userName, string password)
        {
            UserName = userName;
            PassWord = password;
        }

        public string UserName { get; internal set; }

        public string PassWord { get; set; }

        public bool IsActived { get; set; }

        public DateTime CreateDate { get; set; }

        public string AboutMe { get; set; }

        public string ProfileImage { get; set; }

        public static string HashPassword(string password)
        {
            var sha1 = new System.Security.Cryptography.SHA1Managed();
            var src = System.Text.Encoding.ASCII.GetBytes(password);
            var dest = sha1.ComputeHash(src);
            return Convert.ToBase64String(dest);
        }
    }
}
