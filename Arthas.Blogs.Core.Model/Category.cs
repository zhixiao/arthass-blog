﻿using Arthas.Blogs.Infrastructure.Data;
using System.Collections.Generic;

namespace Arthas.Blogs.Core.Model
{
    public class Category : EntityBase<int>
    {
        public string CategoryName { get; set; }

        public int Sequence { get; set; }
    }
}
