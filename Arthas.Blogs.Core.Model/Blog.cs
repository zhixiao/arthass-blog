﻿using Arthas.Blogs.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Model
{
    public class Blog : EntityBase<int>
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Content { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime LastUpdate { get; set; }

        public string DateCategory { get; set; }

        public int ApprovalCount { get; set; }

        public int ReadCount { get; set; }

        public virtual Category Category { get; set; }

        public BlogStatus Status { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }

        public string Tags { get; set; }

        public ICollection<ApproachUser> Approachs { get; set; }
    }

    public enum BlogStatus 
    {
        Draft = 1,
        Normal = 2,
        AlwaysTop = 3
    }
}
