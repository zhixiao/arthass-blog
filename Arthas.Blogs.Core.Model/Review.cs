﻿using Arthas.Blogs.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Model
{
    public class Review : EntityBase<int>
    {
        public string Content { get; set; }

        public DateTime CreateDateTime { get; set; }

        public virtual User Reviewer { get; set; }

        public virtual Blog Blog { get; set; }

        public virtual Review Parent { get; set; }

        public virtual ICollection<ApproachUser> Approachs { get; set; }

        public int ReplyCount { get; set; }
    }
}
