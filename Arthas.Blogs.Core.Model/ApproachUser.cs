﻿using Arthas.Blogs.Infrastructure.Data;

namespace Arthas.Blogs.Core.Model
{
    public class ApproachUser : EntityBase<int>
    {
        public virtual User User { get; set; }

        public Approach Approach { get; set; }
    }

    public enum Approach
    { 
        Agree = 1,
        Disagree = 2
    }
}
