﻿using Arthas.Blogs.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Model
{
    public class Photo : EntityBase<int>
    {
        public string PhotoName { get; set; }

        public string PhotoPath { get; set; }

        public string Description { get; set; }

        public DateTime CreationDate { get; set; }

        public ImageStatus Usefulness { get; set; }
    }

    public enum ImageStatus
    { 
        ForDisplay = 1,
        ForEditor = 2
    }
}
