﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Arthas.Blogs.Infrastructure.Data;

namespace Arthas.Blogs.Core.Model
{
    public class LoginLog : EntityBase<Guid>
    {
        public string IPAddress { get; set; }

        public virtual User User { get; set; }
    }
}
