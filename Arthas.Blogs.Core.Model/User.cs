﻿using Arthas.Blogs.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Model
{
    public class User : EntityBase<int>
    {
        [Required]
        [StringLength(20)]
        public string UserName { get; set; }

        [Required]
        [StringLength(32)]
        public string Password { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        public bool IsActived { get; set; }

        public string ProfileImage { get; set; }

        public ICollection<LoginLog> LoginLogs { get; set; }
    }
}
