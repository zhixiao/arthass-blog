﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using Arthas.Blogs.Core;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Infrastructure.Tool.Exceptions;


namespace Blog.AdminCMS.Controllers
{
    public class CategoryController : ControllerBase
    {
        readonly IDataQuerier DataQuerier;

        public CategoryController(IWebshopBus Bus,IDataQuerier dataQuerier)
        {
            WebshopBus = Bus;
            DataQuerier = dataQuerier;
        }

        public ActionResult Index()
        {
            var result = DataQuerier.GetQuery(CategorySpec.IsNotDelete<Category>());
            var javaScriptSerializer = new JavaScriptSerializer();
            ViewBag.CategoryJson = javaScriptSerializer.Serialize(result);

            return View();
        }


        public JsonResult Create(string name)
        {
            try
            {
                WebshopBus.Send(new CreateCategoryCommand(name));
                return Json(new InfoMessage(true));
            }
            catch (Exception ex)
            {
                return Json(new InfoMessage(false,ex.Message,ex.StackTrace));
            }
        }

        public JsonResult Edit(int ID, string value)
        {
            try
            {
                WebshopBus.Send(new EditCategoryCommand(ID, value));
                return Json(new InfoMessage(true));
            }
            catch (Exception ex)
            {
                return Json(new InfoMessage(false, ex.Message, ex.StackTrace));
            }
        }

        public JsonResult Remove(int id)
        {
            try
            {
                var blogs = DataQuerier.GetQuery(BlogSpec.IsCategory(id));
                if (blogs != null && blogs.Count() > 0) 
                {
                    return Json(new InfoMessage(false, "Can not delete category."));
                }
                WebshopBus.Send(new DeleteCategoryCommand(id));
                return Json(new InfoMessage(true));
            }
            catch (Exception ex)
            {
                return Json(new InfoMessage(false, ex.Message, ex.StackTrace));
            }
        }
    }
}
