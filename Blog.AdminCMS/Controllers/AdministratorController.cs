﻿using AutoMapper;
using Blog.AdminCMS.Models;
using Arthas.Blogs.Core;
using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.AdminCMS.Controllers
{
    public class AdministratorController : ControllerBase
    {
        readonly IDataQuerier DataQuerier;

        public AdministratorController(IDataQuerier dataQuerier,IWebshopBus Bus)
        {
            DataQuerier = dataQuerier;
            WebshopBus = Bus;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateAdministrator model)
        {
            try
            {
                WebshopBus.Send(new CreateAdministratorCommand(model.UserName, model.Password, model.IsActived,model.AboutMe,model.ProfileImage));
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                model.ErrorMessages.Add(ex.Message);

                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            var toEdit = DataQuerier.GetOne(AdministratorSpec.HasID(id));

            var viewModel = Mapper.Map<Administrator, EditAdministrator>(toEdit);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(EditAdministrator viewModel)
        {
            try
            {
                WebshopBus.Send(new EditAdministratorCommand(viewModel.ID, viewModel.Password, viewModel.IsActived, viewModel.AboutMe, viewModel.ProfileImage));
                return RedirectToAction("Index","Administrator");
            }
            catch (Exception ex)
            {
                viewModel.ErrorMessages.Add(ex.Message);

                return View(viewModel);
            }
        }

        [HttpPost]
        public JsonResult Remove(int id)
        {
            try
            {
                WebshopBus.Send(new DeleteAdministratorCommand(id));
                return Json(new { Succ = true });
            }
            catch (Exception ex)
            {
                return Json(new { Succ = false, ErrorMessage = ex.Message, StackTrace = ex.StackTrace });
            }
        }

        public JsonResult Query(string keyword = "",int pageIndex = 0, int pageSize = 10)
        {
            var spec = AdministratorSpec.IsNotDeleted();
            if (string.IsNullOrEmpty(keyword) == false)
            {
                spec = spec.And(AdministratorSpec.HasKeyword(keyword));
            }

            var query = DataQuerier.GetQuery(spec);

            var totalCount = query.Count();

            var result = query.OrderByDescending(it => it.CreateDate).Skip(pageIndex * pageSize).Take(pageSize).ToList().Select(item =>
                new 
                {
                    ID = item.ID,
                    UserName = item.UserName,
                    CreatedDate = item.CreateDate,
                    IsActived = item.IsActived,
                });

            return Json(new { data = result,totalCount = totalCount },JsonRequestBehavior.AllowGet);
        }

        public JsonResult VerifyUserName(string userName)
        {
            var query = DataQuerier.GetQuery(AdministratorSpec.HasUserName(userName));
            if (query.Count() != 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
