﻿using Arthas.Blogs.Infrastructure.Tool.FileStorages;
using Arthas.Blogs.Core;
using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Blog.AdminCMS.Controllers
{
    public class AjaxQueryController : ControllerBase
    {
        readonly IFileStorage FileStorage;
        readonly IDataQuerier DataQuerier;
        private string FileContainer = "Assets";
        private string FilePath = "Image/";

        public AjaxQueryController(IFileStorage fileStorage,IDataQuerier dataQuerier,IWebshopBus Bus)
        {
            WebshopBus = Bus;
            DataQuerier = dataQuerier;
            FileStorage = fileStorage;
        }

        public JsonResult GetImageList(int pageSize = 10, int pageIndex = 0)
        {
            var query = DataQuerier.GetQuery(PhotoSpec.IsUsefulness(Arthas.Blogs.Core.Model.ImageStatus.ForEditor));
            int totalCount = query.Count();
            var result = query.OrderBy(it => it.ID).Skip(pageSize * pageIndex).Take(pageSize).Select(item => new 
            {
                Path = item.PhotoPath,
                Name = item.PhotoName
            });

            return Json(new { totalCount = totalCount, data = result }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBlogReplies(int blogID, int pageSize = 10, int pageIndex = 0)
        {
            var toReply = DataQuerier.GetOne(BlogSpec.IsNotDelete().And(BlogSpec.HasBlogID(blogID)));
            int totalCount = toReply.Reviews.Where(it => it.IsDeleted == false).Count();
            var query = toReply.Reviews.Where(it =>it.IsDeleted == false).OrderByDescending(it => it.CreateDateTime).Skip(pageSize * pageIndex).Take(pageSize).Select(item => 
                new 
                {
                    CreateDate = item.CreateDateTime,
                    Content = item.Content,
                    ReviewerName = item.Reviewer.UserName,
                    ReviewerEmail = item.Reviewer.Email
                }).ToList();
            return Json(new { data = query , totalCount = totalCount },JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Upload()
        {
            try
            {
                var file = Request.Files[0];

                var Uri = FileStorage.Save(file.InputStream, FileContainer, FilePath + Path.GetFileNameWithoutExtension(file.FileName) + Guid.NewGuid() + Path.GetExtension(file.FileName));

                var publicUri = HttpContext.Request.Url.AbsoluteUri
                    .Replace(HttpContext.Request.Url.LocalPath, "")
                    .Replace(":" + HttpContext.Request.Url.Port, "") 
                    + Uri.ToString();

                WebshopBus.Send(new CreatePhotoCommand(
                    Path.GetFileNameWithoutExtension(file.FileName),
                    publicUri, 
                    Arthas.Blogs.Core.Model.ImageStatus.ForEditor));
                //Iframe based uploads(IE and Opera) require a Content-type of text/plain or text/html for the JSON response.
                //they will show an undesired download dialog if the iframe response is set to application/json.
                var headerAcceptVariable = this.Request.Headers["ACCEPT"] as string;

                if (headerAcceptVariable != null && headerAcceptVariable.Contains("application/json"))
                {
                    return Json(new { 
                        Succ = true,
                        Name = file.FileName,
                        Path = publicUri
                    });
                }
                else
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    var response = serializer.Serialize(new {
                        Succ = true,
                        Name = file.FileName,
                        Path = publicUri
                    });

                    return Content(response);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Succ = false, Message = ex.Message });
            }
        }

    }
}
