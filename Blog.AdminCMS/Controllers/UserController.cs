﻿using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.AdminCMS.Controllers
{
    public class UserController : ControllerBase
    {
        readonly IDataQuerier DataQuerier;

        public UserController(IDataQuerier dataQuerier)
        {
            DataQuerier = dataQuerier;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Query(string keyword ="",int pageIndex = 0,int pageSize = 10)
        { 
            var userSpec = UserSpec.IsNotDelete<User>();
            if (string.IsNullOrEmpty(keyword) == false)
            {
                userSpec = userSpec.And(UserSpec.HasKeyword(keyword));
            }

            var userQuery = DataQuerier.GetQuery(userSpec);
            int totalCount = userQuery.Count();

            var result = userQuery.OrderBy(it => it.ID).Skip(pageIndex * pageSize).Take(pageSize).Select(item => new 
            {
                UserName = item.UserName,
                Email = item.Email,
                IsActived = item.IsActived
            });

            return Json(new { data = result,totalCount = totalCount },JsonRequestBehavior.AllowGet);
        }

    }
}
