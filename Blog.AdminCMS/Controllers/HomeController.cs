﻿using Blog.AdminCMS.Models;
using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.AdminCMS.Controllers
{
    public class HomeController : ControllerBase
    {
        readonly IDataQuerier DataQuerier;

        public HomeController(IDataQuerier dataQuerier)
        {
            DataQuerier = dataQuerier;
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
