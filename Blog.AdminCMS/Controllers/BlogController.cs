﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;

using Arthas.Blogs.Core;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Commands;
using Blog.AdminCMS.Models;
using Blog.AdminCMS.Models.Blog;
using Arthas.Blogs.Infrastructure.Tool.Exceptions;
using Arthas.Blogs.Infrastructure.Tool.Web.Paging;
using Arthas.Blogs.Infrastructure.Tool.Helper;

namespace Blog.AdminCMS.Controllers
{
    public class BlogController : ControllerBase
    {
        #region Ctor
        readonly IDataQuerier DataQuerier;

        public BlogController(IWebshopBus Bus,IDataQuerier dataQuerier)
        {
            WebshopBus = Bus;
            DataQuerier = dataQuerier;
        }
        #endregion

        #region Index
        public ActionResult Index(BlogIndexModel indexModel)
        {
            var spec = BlogSpec.IsNotDelete();
            if (string.IsNullOrEmpty(indexModel.Keyword) == false)
            {
                spec = spec.And(BlogSpec.HasKeyword(indexModel.Keyword));
            }
            if (indexModel.CategoryId != 0)
            {
                spec = spec.And(BlogSpec.IsCategory(indexModel.CategoryId));
            }

            var query = DataQuerier.GetQuery(spec);

            indexModel.PagedList = query.OrderByDescending(it => it.CreateDate).Select(it =>
                new BlogListModel()
                {
                    ID = it.ID,
                    Title = it.Title,
                    Category = it.Category.CategoryName,
                    Status = it.Status,
                    ApproveCount = it.ApprovalCount,
                    LastUpdate = it.LastUpdate,
                    RepliesCount = it.ReadCount
                })
                .ToPageList(indexModel.PageIndex, indexModel.PageSize);

            return View(indexModel);
        }
        #endregion

        #region Create
        public ActionResult Create()
        {
            ViewBag.CategoryJson = JsonDotNetHelper.ConvertToJson(DataQuerier.GetQuery(
                CategorySpec.IsNotDelete<Category>())
                .Select(item =>
                    new 
                    {
                        ID = item.ID,
                        Name = item.CategoryName
                    })
            );
            ViewBag.BlogStatus = JsonDotNetHelper.EnumConvertToJson(typeof(BlogStatus));
            return View(new CreateBlogModel());
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Create(CreateBlogModel viewModel)
        {
            try
            {
                var status = (BlogStatus)viewModel.StatusValue;
                WebshopBus.Send(new CreateBlogCommand(viewModel.Title, viewModel.Description, viewModel.Content, viewModel.CategoryID, viewModel.Tags, status));
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                viewModel.ErrorMessages.Add(ex.Message);
                return View(viewModel);
            }
        }
        #endregion

        #region Edit
        public ActionResult Edit(int id)
        {
            var domianModel = DataQuerier.GetByID<Arthas.Blogs.Core.Model.Blog,int>(id);
            var viewModel = Mapper.Map<Arthas.Blogs.Core.Model.Blog, EditBlogModel>(domianModel);

            ViewBag.CategoryJson = JsonDotNetHelper.ConvertToJson(DataQuerier.GetQuery(
             CategorySpec.IsNotDelete<Category>())
             .Select(item =>
                 new
                 {
                     ID = item.ID,
                     Name = item.CategoryName
                 })
            );
            ViewBag.BlogStatus = JsonDotNetHelper.EnumConvertToJson(typeof(BlogStatus));

            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Edit(EditBlogModel viewModel)
        {
            try
            {
                var status = (BlogStatus)viewModel.StatusValue;
                WebshopBus.Send(new EditBlogCommand(viewModel.ID, viewModel.Title, viewModel.Description, viewModel.Content, viewModel.CategoryID, viewModel.Tags, status));
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                viewModel.ErrorMessages.Add(ex.Message);
                return View(viewModel);
            }
        }
        #endregion

        #region Delete
        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                WebshopBus.Send(new DeleteBlogCommand(id));
                return Json(new InfoMessage(true));
            }
            catch (Exception ex)
            {
                return Json(new InfoMessage(false, ex.Message, ex.StackTrace));
            }
        }
        #endregion 

        #region Ajax
        public JsonResult Query(string keyword = "", int catetoryid = 0, int pageIndex = 0, int pageSize = 10)
        {
            var spec = BlogSpec.IsNotDelete();
            if (string.IsNullOrEmpty(keyword) == false)
            {
                spec = spec.And(BlogSpec.HasKeyword(keyword));
            }
            if (catetoryid != 0)
            {
                spec = spec.And(BlogSpec.IsCategory(catetoryid));
            }

            var query = DataQuerier.GetQuery(spec);

            var totalCount = query.Count();
            var list = query.OrderByDescending(it => it.CreateDate).ThenByDescending(it => it.Status).Skip(pageIndex * pageSize).Take(pageSize).ToList();
            var result = list.Select(item => new
            {
                ID = item.ID,
                Title = item.Title,
                Category = item.Category.CategoryName,
                Status = Enum.GetName(typeof(BlogStatus),item.Status) ?? "",
                ApproveCount = item.ApprovalCount,
                LastUpdate = item.LastUpdate.ToString("yyyy-MM-dd HH:mm"),
                RepliesCount = item.Reviews == null ? 0 : item.Reviews.Where(rv => rv.IsDeleted == false).Count()
            });

            return Json(new { data = result , totalCount = totalCount },JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReviews(int BlogID)
        {
            var blog = DataQuerier.GetByID<Arthas.Blogs.Core.Model.Blog, int>(BlogID);
            var reviews = blog.Reviews.Where(rv => rv.IsDeleted == false).Select(item => new 
            {
                Reviewer = item.Reviewer.UserName,
                ReviewerImage = item.Reviewer.ProfileImage,
                Content = item.Content
            });
            return Json(reviews, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
