﻿using Blog.AdminCMS.Models;
using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Blog.AdminCMS.Controllers
{
    public class AccountController : ControllerBase
    {
        readonly IDataQuerier DataQuerier;

        public AccountController(IDataQuerier dataQuerier)
        {
            DataQuerier = dataQuerier;
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var administrator = DataQuerier.GetOne(AccountSpec.HasUserName(model.UserName).And(AdministratorSpec.IsNotDeleted()));
                if (administrator != null && administrator.PassWord == Administrator.HashPassword(model.Password))
                {
                    FormsAuthentication.SetAuthCookie(administrator.UserName, false);
                    return RedirectToAction("Index", "Blog");
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect user name or password");
                }
            }
            else
            {
                ModelState.AddModelError("", "IsValid is false");
            }
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

    }
}
