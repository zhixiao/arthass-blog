﻿using AutoMapper;
using Blog.AdminCMS.Models;
using Arthas.Blogs.Core.Model;
using System.Linq;

namespace Blog.AdminCMS.App_Start
{
    public static class AutoMapperConfig
    {
        public static void MapModels()
        {
            Mapper.CreateMap<Administrator, EditAdministrator>()
                .ForMember(d => d.Password, opt => opt.Ignore())
                .ForMember(d => d.ConfirmPassword, opt => opt.Ignore())
                .IgnoreAllNoneExisting();

            #region Blog
            Mapper.CreateMap<Arthas.Blogs.Core.Model.Blog, EditBlogModel>()
                .ForMember(d => d.CategoryID, opt => opt.MapFrom(source => source.Category.ID))
                .IgnoreAllNoneExisting();
            #endregion
        }
    }

    static class AutoMapperExtension
    {
        public static IMappingExpression<TSource, TDestination> 
            IgnoreAllNoneExisting<TSource, TDestination>(this IMappingExpression<TSource, TDestination> expression)
        {
            var sourceType = typeof(TSource);
            var destinationType = typeof(TDestination);
            var existingMaps = Mapper.GetAllTypeMaps().First(x => x.SourceType.Equals(sourceType) && x.DestinationType.Equals(destinationType));
            foreach (var property in existingMaps.GetUnmappedPropertyNames())
            {
                expression.ForMember(property, opt => opt.Ignore());
            }
            return expression;
        }
    }
}