﻿using Arthas.Blogs.Infrastructure.Tool.Web.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.AdminCMS.Models.Blog
{
    public class BlogIndexModel : IndexModelBase
    {
        public PagedList<BlogListModel> PagedList { get; set; }

        public int CategoryId { get; set; }
    }
}