﻿using Arthas.Blogs.Core.Model;
using System.ComponentModel.DataAnnotations;

namespace Blog.AdminCMS.Models
{
    public class CreateBlogModel : ModelBase
    {
        public CreateBlogModel()
        {
            Status = BlogStatus.Normal;
        }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        public string Content { get; set; }

        public int CategoryID { get; set; }

        [Required]
        public string Tags { get; set; }

        public BlogStatus Status { get; set; }

        public int StatusValue { get; set; }
    }

    public class EditBlogModel : CreateBlogModel
    {
        public int ID { get; set; }
    }
}