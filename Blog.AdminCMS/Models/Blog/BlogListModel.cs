﻿using Arthas.Blogs.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.AdminCMS.Models
{
    public class BlogListModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Category { get; set; }

        public BlogStatus Status { get; set; }

        public int ApproveCount { get; set; }

        public DateTime LastUpdate { get; set; }

        public int RepliesCount { get; set; }
    }
}