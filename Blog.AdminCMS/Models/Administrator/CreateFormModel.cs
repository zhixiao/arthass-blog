﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.AdminCMS.Models
{
    public class CreateAdministrator : ModelBase
    {
        [Required]
        [System.Web.Mvc.Remote("VerifyUserName","Administrator",ErrorMessage="UserName is already exist.")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(int.MaxValue, MinimumLength = 6, ErrorMessage = "Password cannot be less than 6 characters.")]
        public string Password { get; set; }

        [Required]
        [Compare("Password",ErrorMessage="Two password must be same.")]
        public string ConfirmPassword { get; set; }

        public bool IsActived { get; set; }

        public string AboutMe { get; set; }

        public string ProfileImage { get; set; }
    }

    public class EditAdministrator : CreateAdministrator
    {
        public int ID { get; set; }
    }
}
