﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.AdminCMS.Models
{
    public class IndexModelBase : ModelBase
    {
        public string Keyword { get; set; }

        public string SortField { get; set; }

        public string SortDirection { get; set; }

        public int? PageIndex { get; set; }

        public int? PageSize { get; set; }
    }
}