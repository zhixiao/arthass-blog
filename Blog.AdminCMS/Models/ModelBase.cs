﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.AdminCMS.Models
{
    public class ModelBase
    {
        public ModelBase()
        {
            ErrorMessages = new List<string>();
        }

        public List<string> ErrorMessages { get; set; }
    }
}