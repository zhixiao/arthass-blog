﻿//{
//    data: options.data,
//    columns: options.columns,
//    onSorting: options.onSorting,
//    cssClass: options.cssClass,
//    template: options.template,
//    onEditing: options.onEditing,
//    onDeleting: options.onDeleting,
//    onDetail: options.onDetail,
//    customEvents: options.customEvents,
//    customProperties: options.customProperties
//}
//{
//    totalCount: options.pagingOptions.totalCount,
//    pageSize: options.pagingOptions.pageSize,
//    currentPageIndex: options.pagingOptions.currentPageIndex,
//    onPaging: options.pagingOptions.onPaging,
//    template: options.pagingOptions.template,
//    pageIndexRange: options.pagingOptions.pageIndexRange,
//    selectorVisible: options.pagingOptions.selectorVisible
//}
(function ($) {
    //-----------private function----------------
    function newGuid() {
        var guid = "";
        for (var i = 1; i <= 32; i++) {
            var n = Math.floor(Math.random() * 16.0).toString(16);
            guid += n;
            if ((i == 8) || (i == 12) || (i == 16) || (i == 20))
                guid += "-";
        }
        return guid;
    }

    function getColunmsFromData(data) {
        if ((typeof data.length !== 'number') || data.length === 0) {
            return [];
        }
        var columns = [];
        for (var propertyName in data[0]) {
            columns.push({ headerText: propertyName, field: propertyName, sortable: propertyName });
        }
        return columns;
    }

    function InitializationColumns(columns) {
        var result = [];
        var ContainerId = CreateTemplateContainer();
        for (var key in columns) {
            var item = columns[key];
            //init

            if (item.formatter == undefined) {
                if (item.type == undefined) {
                    item.type = "normal";
                }
            } else {
                item.type = "normalHtml"
            }
            if (item.headerText == undefined) item.headerText = "";
            if (item.field == undefined) item.field = "";
            if (item.sortable == undefined) item.sortable = "false";
            item.columnId = item.field + "_" + newGuid();
            item.headerId = item.field + "_" + newGuid();
            item.type = item.type.toLowerCase();
            if (item.headerTemplate != undefined) {
                item.headerTemplate = ConvertHeaderTemplate(item.headerTemplate) || _defaultHeaderTemplate;
            } else {
                item.headerTemplate = _defaultHeaderTemplate;
            }

            if (item.template != undefined) {
                item.template = ConverteCustomTemplate(item);
                result.push(item);
                AddTemplateToContainer(item.columnId, item.template, ContainerId);
            } else {
                item.template = defaultColumnTemplates[item.type];
                result.push(item);
                AddTemplateToContainer(item.columnId, item.template, ContainerId);
            }
            AddTemplateToContainer(item.headerId, item.headerTemplate, ContainerId);
        }
        return result;
    }

    function AddTemplateToContainer(id, template, containerId) {
        var script = document.createElement("script");
        script.type = "text/html";
        script.id = id;
        script.text = template;
        document.getElementById(containerId).appendChild(script);
    }

    function CreateTemplateContainer() {
        var div = document.createElement("div");
        div.id = newGuid();
        document.body.appendChild(div);
        return div.id;
    }

    function ConverteCustomTemplate(column) {
        while (column.template.indexOf("$data") != -1) { column.template = column.template.replace("$data", "$parent"); }
        while (column.template.indexOf("$column") != -1) { column.template = column.template.replace("$column", "$data"); }
        if (column.type == "edit") {
            return "<div data-bind=\"click: function(){$root.onEditing($parent)} \" style=\"display: inline-block;\">" + column.template + "</div>";
        } else {
            if (column.type == "delete") {
                return "<div data-bind=\"click: function(){$root.onDeleting($parent)}\" style=\"display: inline-block;\">" + column.template + "</div>";
            } else {
                if (column.type == "detail") {
                    return "<div data-bind=\"click: function(){$root.onDetail($parent)}\" style=\"display: inline-block;\">" + column.template + "</div>";
                } else {
                    return column.template;
                }
            }
        }
    }

    function ConvertHeaderTemplate(template) {
        while (template.indexOf("$data") != -1) { template = template.replace("$data", "$parent"); }
        while (template.indexOf("$column") != -1) { template = template.replace("$column", "$data"); }
        return template;
    }
    //----------private default template------------------------------------------------------------
    var _defaultdataGridTemplate = "\
                    <table data-bind=\"attr:{ 'class': cssClass }\" id=\"defaultGrid\">\
                        <thead>\
                            <tr>\
                                  <!-- ko foreach: columns -->\
                                      <!-- ko ifnot: $data.sortable == true -->\
                                          <th data-bind=\"template:$data.headerId,attr: {width:$data.headerWidth}\"></th>\
                                      <!-- /ko -->\
                                     <!-- ko if: $data.sortable == true -->\
                                         <th data-bind=\"template:$data.headerId,click: $root.onSortingClick,attr: {width:$data.headerWidth}\"></th>\
                                     <!-- /ko -->\
                                  <!-- /ko -->\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: data\">\
                           <tr>\
                                <!-- ko foreach: $parent.columns -->\
                                       <td data-bind=\" attr:{ 'class': $data.tdClass },template:$data.columnId \"></td>\
                                <!-- /ko -->\
                            </tr>\
                        </tbody>\
                    </table>";

    var _defaultPagingTemplate = "\
                            <ul class=\"pagination\">\
                            <!-- ko if:currentPageIndex() === 0 -->\
                            <li class=\"disabled\" >\
                                <a href=\"javascript://\">&laquo;&laquo;</a></li>\
                            <!-- /ko -->\
                            <!-- ko ifnot:currentPageIndex() === 0 -->\
                            <li class=\"backward\">\
                                <a href=\"javascript://\" data-bind=\"click:  function(){ currentPageIndex(0); }\">&laquo;&laquo;</a></li>\
                            <!-- /ko -->\
                            <!-- ko if:hasPrevPage -->\
                            <li class=\"previous\">\
                                <a href=\"javascript://\" data-bind=\"click: function(){ currentPageIndex(currentPageIndex() - 1); }\">&laquo;</a></li>\
                            <!-- /ko -->\
                            <!-- ko ifnot:hasPrevPage -->\
                            <li class=\"disabled previous\">\
                                <a href=\"javascript://\" class=\"disabledPage\">&laquo;</a></li>\
                            <!-- /ko -->\
                            <!-- ko foreach:pages -->\
                            <li data-bind=\"css: { active: active }\">\
                                <a href=\"javascript://\" data-bind=\"text: index+1, click: function(i) { if(!i.active){ $root.currentPageIndex(i.index); } } \"></a>\
                            </li>\
                             <!-- /ko -->\
                            <!-- ko if:hasNextPage --><li class='next'>\
                                <a href=\"javascript://\" data-bind=\"click: function(){ currentPageIndex(currentPageIndex() + 1); }\">&raquo;</a></li>\
                            <!-- /ko -->\
                            <!-- ko ifnot:hasNextPage -->\
                            <li class=\"disabled next\">\
                                <a href=\"javascript://\" class=\"disabledPage\">&raquo;</a></li>\
                            <!-- /ko -->\
                            <!-- ko ifnot:currentPageIndex() === (pageCount()-1) || pageCount() == 0-->\
                            <li class='forward'>\
                                <a href=\"javascript://\" data-bind=\"click: function(){ currentPageIndex(pageCount()-1); }\">&raquo;&raquo;</a></li>\
                            <!-- /ko -->\
                            <!-- ko if:currentPageIndex() === (pageCount()-1) || pageCount() == 0-->\
                            <li class=\"disabled forward\">\
                                <a href=\"javascript://\" class=\"disabledPage\">&raquo;&raquo;</a></li>\
                            <!-- /ko -->\
                    </ul>";

    var _defaultEditingTemplate = "<a class='icon-wrapper' href=\"javascript://\" data-bind=\"click: function(){$root.onEditing($parent)}\"><span class=\"glyphicon glyphicon-pencil\"></span></a>";
    var _defaultDeletingTemplate = "<a  href=\"javascript://\" data-bind=\"click: function(){$root.onDeleting($parent)}\"><i class=\"glyphicon glyphicon-remove\"></i></a>";
    var _defaultDetailTemplate = "<a href=\"javascript://\" data-bind=\"click: function(){$root.onDetail($parent)}\"><i class=\"glyphicon glyphicon-list-alt\"></i></a>";
    var _defaultNormalTextTemplate = "<p data-bind=\"text:　 $data.formatter == undefined?  $parent[$data.field] : formatter($parent)\"></p>";
    var _defaultNormalHtmlTemplate = "<p data-bind=\"html:　 $data.formatter == undefined?  $parent[$data.field] : formatter($parent)\"></p>";
    var _defaultHeaderTemplate = "<span data-bind=\"html: headerText\"></span>";

    var defaultColumnTemplates = { "edit": _defaultEditingTemplate, "delete": _defaultDeletingTemplate, "detail": _defaultDetailTemplate, "normal": _defaultNormalTextTemplate, "normalhtml": _defaultNormalHtmlTemplate };

    // Templates used to render the grid and pagination
    var templateEngines = new ko.nativeTemplateEngine();

    templateEngines.addTemplate = function (templateName, templateMarkup) {
        var script = document.createElement("script");
        script.type = "text/html";
        script.id = templateName;
        script.text = templateMarkup;
        document.body.appendChild(script);
    };

    ko.grid = {
        viewModel: function (vmOptions) {
            var self = this;

            self.data = vmOptions.data;
            self.columns = InitializationColumns(vmOptions.columns) || getColunmsFromData(ko.utils.unwrapObservable(vmOptions.data));
            self.cssClass = vmOptions.cssClass || 'table table-hover';
            self.template = vmOptions.template || _defaultdataGridTemplate;
            self.onSortingClick = function (item, event) {
                var sortable = (typeof item.sortable == 'undefined' || item.sortable == false ? false : true);
                if (sortable) {
                    var direction = 'asc';
                    var target = $(event.target);

                    $("th", target.parent()).removeClass('sort-asc sort-desc');
                    if (typeof target.attr('direction') != 'undefined') {
                        if (target.attr('direction') != 'asc') {
                            direction = 'asc';
                            target.addClass('sort-asc');
                        }
                        else {
                            direction = 'desc';
                            target.addClass('sort-desc');
                        }
                    }
                    else {
                        target.addClass('sort-asc');
                    }
                    target.attr('direction', direction);

                    if (vmOptions.onSorting != undefined) {
                        vmOptions.onSorting(item, direction);
                    }
                }
            };
            self.onEditing = function (data) {
                vmOptions.onEditing(data);
            };
            self.onDeleting = function (data) {
                vmOptions.onDeleting(data);
            };
            self.onDetail = function (data) {
                vmOptions.onDetail(data);
            };
            self.onCustomEvent = function (functionName) {
                var fn = vmOptions.customEvents[functionName];
                var args = Array.prototype.slice.call(arguments, 1);
                fn.apply(this, args);
            };
            self.customProperties = vmOptions.customProperties;
        },
        pagingviewModel: function (options) {
            var self = this;
            self.totalCount = options.totalCount || ko.observable(0);
            self.pageSize = ko.observable(10);
            self.currentPageIndex = options.currentPageIndex || ko.observable(0);
            self.pageIndexRange = options.pageIndexRange || 6;
            self.pagingTempalte = options.template || _defaultPagingTemplate;
            self.pageSizeSelectorVisible = options.pageSizeSelectorVisible || ko.observable(true);

            var _previousCount = 0;
            self.totalCount.subscribe(function (oldValue) {
                _previousCount = oldValue;
            }, this, "beforeChange");
            self.totalCount.subscribe(function (newValue) {
                if (_previousCount != 0) {
                    self.currentPageIndex(0);
                }
            });

            self.pageCount = ko.computed(function () {
                var size = self.pageSize();
                var total = self.totalCount();
                return Math.ceil(total / size);
            }, self);

            self.pages = ko.computed(function () {
                var pageFrom = Math.max(0, self.currentPageIndex() - self.pageIndexRange / 2);
                var pageTo = Math.min(self.pageCount(), self.currentPageIndex() + self.pageIndexRange / 2);
                pageFrom = Math.max(0, Math.min(pageTo - self.pageIndexRange, pageFrom));
                pageTo = Math.min(self.pageCount(), Math.max(pageFrom + self.pageIndexRange, pageTo));
                var pages = [];
                for (var i = pageFrom; i < pageTo; i++) {
                    pages.push({ index: i, active: i == self.currentPageIndex() });
                }
                return pages;
            });


            self.hasNextPage = ko.computed(function () {
                return self.currentPageIndex() < self.pageCount() - 1;
            });

            self.hasPrevPage = ko.computed(function () {
                return self.currentPageIndex() > 0;
            });

            self.onPaging = function (index) {
                $("#defaultGrid").addClass("loading");
                options.onPaging(index, self.pageSize());
            }

            var oldIndex = 0;
            self.currentPageIndex.subscribe(function (newvalue) {
                if (newvalue != oldIndex && newvalue != undefined) {
                    self.onPaging(newvalue);
                    oldIndex = newvalue;
                }
            });

            self.selectedPageSize = ko.observable(10);
            self.selectedPageSize.subscribe(function (newvalue) {
                self.pageSize(newvalue);
                self.currentPageIndex(0);
                self.onPaging(self.currentPageIndex(), self.pageSize());
            });
            self.allPageSizes = ko.computed(function () {
                var pageSizes = [ 10, 20, 30, 40, 50];
                return pageSizes;
            });
        }
    };

    $.fn.dataGrid = function (options) {
        var element = $(this);
        while (element.firstChild)
            ko.removeNode(element.firstChild);

        var gridTemplateName = "ko_grid" + newGuid();
        var paginationTemplateName = "ko_grid_pageLinks" + newGuid();
        var uponPagingTemplateName = "ko_grid_uponPageLinks" + newGuid();

        var gridViewModel = new ko.grid.viewModel(options);

        var pagingViewModel = undefined;
        if (options.pagingOptions != undefined) {
            pagingViewModel = new ko.grid.pagingviewModel(options.pagingOptions);
            if (options.pagingOptions.uponPaginationVisible == true) {
                templateEngines.addTemplate(uponPagingTemplateName, pagingViewModel.pagingTempalte);
                var uponPagingContainer = element[0].appendChild(document.createElement("div"));
                uponPagingContainer.className += " list-menu";
                uponPagingContainer.className += " clearfix";
                uponPagingContainer.id = "uponPagingContainer";
                ko.renderTemplate(uponPagingTemplateName, pagingViewModel, { templateEngine: templateEngines }, uponPagingContainer, "replaceChildren");
            }
        }

        //init template engines
        templateEngines.addTemplate(gridTemplateName, gridViewModel.template);
        //render dataGrid
        var dataGridContainer = element[0].appendChild(document.createElement("div"));
        ko.renderTemplate(gridTemplateName, gridViewModel, { templateEngine: templateEngines }, dataGridContainer, "replaceNode");
        
        if (options.pagingOptions != undefined) {
            templateEngines.addTemplate(paginationTemplateName, pagingViewModel.pagingTempalte);
            var paginationContainer = element[0].appendChild(document.createElement("div"));
            paginationContainer.className += " list-menu";
            paginationContainer.className += " clearfix";
            ko.renderTemplate(paginationTemplateName, pagingViewModel, { templateEngine: templateEngines }, paginationContainer, "replaceChildren");
            pagingViewModel.onPaging(pagingViewModel.currentPageIndex() || 0 , pagingViewModel.selectedPageSize());
        }
    };
})(jQuery)