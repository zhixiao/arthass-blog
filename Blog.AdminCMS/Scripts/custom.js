﻿var Blog = (function (didixbank) {
    var self = didixbank;

    self.ControlPanel = {
        Controls: {}
    };

    self.ControlPanel.Url = {};

    return self;
}(Blog || {}));

Blog.ControlPanel.Url = (function (adminCMS) {
    var self = adminCMS;

    self.UpdateParameterList = function (url, paramList) {
        $.each(paramList, function (key, value) {
            url = self.updateURLParameter(url, key, value);
        })
        return url;
    };

    self.replaceUrlParameters = function (url, paramList) {
        var hash;
        if (url.indexOf("#") > 0) {
            hash = url.substring(url.indexOf("#"));
            url = url.substring(0, url.indexOf("#"));
        }

        var tempArray = url.split("?");
        var baseURL = tempArray[0];
        url = baseURL;
        url += "?";
        $.each(paramList, function (key, value) {
            url += key + "=" + value + "&";
        })

        if (hash) {
            return url + hash;
        } else {
            return url;
        }
    };

    //function to rewrite url params value
    self.updateURLParameter = function (url, param, paramVal) {
        var hash;
        if (url.indexOf("#") > 0) {
            hash = url.substring(url.indexOf("#"));
            url = url.substring(0, url.indexOf("#"));
        }

        var newAdditionalURL = "";
        var tempArray = url.split("?");
        var baseURL = tempArray[0];
        var additionalURL = tempArray[1];
        var temp = "";
        if (additionalURL) {
            tempArray = additionalURL.split("&");
            for (i = 0; i < tempArray.length; i++) {
                if (tempArray[i].split('=')[0] != param) {
                    newAdditionalURL += temp + tempArray[i];
                    temp = "&";
                }
            }
        }

        var rows_txt = temp + "" + param + "=" + paramVal;

        if (hash) {
            return baseURL + "?" + newAdditionalURL + rows_txt + hash;
        } else {
            return baseURL + "?" + newAdditionalURL + rows_txt;
        }
    };


    return self;
})(Blog.ControlPanel.Url || {});

$(function () {
    $(document).ajaxComplete(function (event, xhr, settings) {
        var loadingtr = $(".loading");
        loadingtr.removeClass("loading");
    });

    $(".pagination .pagenum a").each(function () {
        var urlParameters = {
            "PageIndex": $(this).attr("pager-value")
        };
        var pageurl = Blog.ControlPanel.Url.UpdateParameterList(window.location.href, urlParameters);
        $(this).attr("href", pageurl);
    });
})

$(function ($) {
    $(".js-delete").click(function (e) {
        var selectedId = $(this).attr("data-id");
        var jsonObject = tryParseJSON(selectedId);
        var url = $(this).data("url");

        $.confirm({
            text: "Are you sure you want to delete this item?",
            confirm: function (button) {
                if (!jsonObject) {
                    $.post(url, { id: selectedId }, function (result) {
                        if (result.Success == true) {
                            window.location.reload();
                        } else {
                            Didix.ControlPanel.ShowMsg.error(result.Message);
                        }
                    });
                } else {
                    $.post(url, jsonObject, function (result) {
                        if (result.Success == true) {
                            window.location.reload();
                        } else {
                            Didix.ControlPanel.ShowMsg.error(result.Message);
                        }
                    });
                }
            },
            cancel: function (button) {
            }
        });
    });

    function tryParseJSON(jsonString) {
        try {
            var o = JSON.parse(jsonString);

            // Handle non-exception-throwing cases:
            // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
            // but... JSON.parse(null) returns 'null', and typeof null === "object", 
            // so we must check for that, too.
            if (o && typeof o === "object" && o !== null) {
                return o;
            }
        }
        catch (e) { }

        return false;
    };

    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    $("#js-keyword").val(getUrlParameter("keyword"));

    $("#js-search").click(function () {
        var urlParameters = {
            "keyword": $("#js-keyword").val()
        };

        window.location.href = Blog.ControlPanel.Url.replaceUrlParameters(window.location.href, urlParameters);
    });
});