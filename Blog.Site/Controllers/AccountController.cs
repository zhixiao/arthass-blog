﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using Blog.Site.Models;
using Arthas.Blogs.Infrastructure.Tool.Helper;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using Blog.Site.Helper;
using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core;

namespace Blog.Site.Controllers
{
    public class AccountController : ControllerBase
    {
        readonly IDataQuerier DataQuerier;

        public AccountController(IDataQuerier dataQuerier,IWebshopBus Bus)
        {
            DataQuerier = dataQuerier;
            WebshopBus = Bus;
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            var user = DataQuerier.GetOne(UserSpec.IsEmail(model.Email));
            if (user == null)
            {
                ViewBag.ErrorMessage = "无此用户";
            }
            else if (user.Password == PasswordHelper.HashPassword(model.Password))
            {
                BlogSiteContext.SetAuthTicketCookie(user.ID, user.UserName, false);
            }
            else
            {
                ViewBag.ErrorMessage = "密码错误";
            }
            if (string.IsNullOrEmpty(model.ReturnUrl))
            {
                return RedirectToAction("Index", "Blog");
            }
            return Redirect(model.ReturnUrl);
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Blog");
        }

        public ActionResult RegisterForm()
        {
            return View(new RegisterUserModel());
        }

        [HttpPost]
        public ActionResult RegisterForm(RegisterUserModel model)
        {
            WebshopBus.Send(new CreateUserCommand(
                new User() 
                {
                    Email = model.Email,
                    UserName = model.UserName,
                    Password = PasswordHelper.HashPassword(model.Password),
                    IsActived = true,
                    IsDeleted = false,
                    ProfileImage = string.IsNullOrEmpty(model.ProfileImage) ? Url.Content("~/Content/images/gravatar.jpg") : model.ProfileImage,
                    LoginLogs = new List<LoginLog>()
                }));
            var query = DataQuerier.GetQuery(UserSpec.IsEmail(model.Email).And(UserSpec.IsNotDelete<User>())).FirstOrDefault();
            if (query != null)
            {
                BlogSiteContext.SetAuthTicketCookie(query.ID, query.UserName, false);
            }
            ViewBag.LoginSuccess = true;
            return View();
        }

        public JsonResult VerifyUserName(string UserName)
        {
            var result = DataQuerier.GetQuery(UserSpec.IsNotDelete<Arthas.Blogs.Core.Model.User>().And(UserSpec.IsUserName(UserName)));
            if (result.Count() > 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VerifyEmail(string email)
        {
            var result = DataQuerier.GetQuery(UserSpec.IsNotDelete<Arthas.Blogs.Core.Model.User>().And(UserSpec.IsEmail(email)));
            if (result.Count() > 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
