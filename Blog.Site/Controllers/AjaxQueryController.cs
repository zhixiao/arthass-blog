﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Blog.Site.Models;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using Arthas.Blogs.Core.Model;
using System.Web.Script.Serialization;
using Blog.Site.Helper;

namespace Blog.Site.Controllers
{
    public class AjaxQueryController : Controller
    {
        readonly IDataQuerier DataQuerier;

        public AjaxQueryController(IDataQuerier dataQuerier)
        {
            DataQuerier = dataQuerier;
        }

        public JsonResult GetSideBarInfo()
        {
            var Profile = DataQuerier.GetQuery(AdministratorSpec.HasID(1)).Select(it => new { ProfileImage = it.ProfileImage, AboutMe = it.AboutMe });

            var tagStrings = DataQuerier.GetQuery(BlogSpec.IsNotDelete()).Select(it =>  it.Tags ).ToList();

            var Tags = GetTags(tagStrings);

            var MostPopular = DataQuerier.GetQuery(BlogSpec.IsNotDelete()).OrderByDescending(it => it.ReadCount).Take(5).ToList().Select(it =>
                new { ID = it.ID,Title = it.Title,PostDate = it.CreateDate.ToString("MMMM dd,yyyy") });

            var RecentlyComment = DataQuerier.GetQuery(ReviewSpec.IsNotDelete<Review>()).OrderByDescending(it =>it.CreateDateTime).Take(5).ToList()
                .Select(it => new { 
                    ID = it.ID, 
                    Thumbnail = it.Content.Length > 20 ? it.Content.Substring(0,20) : it.Content,
                    Author = it.Reviewer.UserName});

            return Json(new {
                Profile = Profile,
                Tags = Tags,
                MostPopular = MostPopular,
                RecentlyComment = RecentlyComment
            }, JsonRequestBehavior.AllowGet);
        }

        private List<string> GetTags(List<string> tagstrings)
        {
            var tags = new List<string>();

            foreach (var str in tagstrings)
            {
                var _tags = str.Split(new char[]{ ',' },StringSplitOptions.RemoveEmptyEntries);
                foreach (var _tag in _tags)
                {
                    tags.Add(_tag);
                }
            }

            return tags;
        }


        public JsonResult GetReviews(int id)
        {
            var query = DataQuerier.GetQuery(ReviewSpec.IsNotDelete<Review>().And(ReviewSpec.HasBlogID(id)).And(ReviewSpec.IsRootComment()));

            var totalCount = DataQuerier.GetQuery(ReviewSpec.IsNotDelete<Review>().And(ReviewSpec.HasBlogID(id))).Count();

            var comments = query.ToList().Select(it => 
                new ReviewItem()
                { 
                    comment_id = it.ID,
                    parent_id = it.Parent == null ? 0 : it.Parent.ID,
                    VirtualReview = it,
                    in_reply_to = it.Parent != null ? it.Parent.Reviewer.UserName : string.Empty,
                    created_by = it.Reviewer.ID,
                    fullname = it.Reviewer.UserName,
                    picture = it.Reviewer.ProfileImage,
                    posted_date = it.CreateDateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    text = it.Content,
                    attachments = new List<string>(),
                }).ToList();

            foreach (var item in comments)
            {
                GetCommentTree(item);
            }

            var user = new CommentUserModel();
            //default image
            user.picture = Url.Content("~/Content/images/gravatar.jpg");
            if (User.Identity.IsAuthenticated == true)
            {
                var LoginUser = DataQuerier.GetByID<Arthas.Blogs.Core.Model.User, int>(BlogSiteContext.CurrentUser.UserID);
                user.user_id = LoginUser.ID;
                user.fullname = LoginUser.UserName;
                user.picture = string.IsNullOrEmpty(LoginUser.ProfileImage) ? Url.Content("~/Content/images/gravatar.jpg") : LoginUser.ProfileImage;
                user.is_logged_in = true;
                user.is_add_allowed = true;
                user.is_edit_allowed = true;
            }

            var results = new { comments = comments, total_comment = totalCount, user = user };

            return Json(new { results = results }, JsonRequestBehavior.AllowGet);
        }

        private void GetCommentTree(ReviewItem comment)
        {
            var children = DataQuerier.GetQuery(ReviewSpec.IsNotDelete<Review>().And(ReviewSpec.HasParentID(comment.comment_id))).ToList();
            if (children.Count() > 0)
            {
                foreach (var item in children)
                {
                    var newComment = new ReviewItem() 
                    {
                        comment_id = item.ID,
                        parent_id = item.Parent == null ? 0 : item.Parent.ID,
                        VirtualReview = item,
                        in_reply_to = item.Parent != null ? item.Parent.Reviewer.UserName : string.Empty,
                        created_by = item.Reviewer.ID,
                        fullname = item.Reviewer.UserName,
                        picture = item.Reviewer.ProfileImage,
                        posted_date = item.CreateDateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                        text = item.Content,
                        attachments = new List<string>(),
                    };
                    GetCommentTree(newComment);
                    if (comment.childrens == null)
                    {
                        comment.childrens = new List<ReviewItem>();
                    }
                    comment.childrens.Add(newComment);
                }
            }
        }
    }
}
