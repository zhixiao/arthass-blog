﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Site.Controllers
{
    using Blog.Site.Models;
    using Arthas.Blogs.Core.Queries;
    using Arthas.Blogs.Core.Queries.Specifications;

    public class SearchController : Controller
    {
        readonly IDataQuerier DataQuerier;

        public SearchController(IDataQuerier dataQuerier)
        {
            DataQuerier = dataQuerier;
        }

        public ActionResult Index(string keyword)
        {
            keyword = keyword.Trim();

            if (string.IsNullOrEmpty(keyword))
            {
                return Redirect(Request.UrlReferrer.ToString());
            }

            ViewBag.Keyword = keyword;

            return View();
        }

        public JsonResult GetQuery(string keyword = "")
        {
            var spec = BlogSpec.IsNotDelete();
            if (string.IsNullOrEmpty(keyword) == false)
            {
                keyword = keyword.Trim();
                spec = spec.And(BlogSpec.HasKeyword(keyword));
            }

            var query = DataQuerier.GetQuery(spec);

            int totalCount = query.Count();
            var baseUrl = Url.Action("Index", "Search");
            var result = query.OrderByDescending(it => it.CreateDate).ToList().Select(item =>
                new SearchItem
                {
                    ID = item.ID,
                    CreateDate = item.CreateDate.ToString("MMM dd , yyyy"),
                    Description = item.Description,
                    TagString = item.Tags,
                    TagUrlBase = baseUrl,
                    Title = item.Title
                });

            return Json(new { totalCount = totalCount , data = result }, JsonRequestBehavior.AllowGet);
        }

    }
}
