﻿using Arthas.Blogs.Infrastructure.Tool.Helper;
using Arthas.Blogs.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Site.Controllers
{
    public class ControllerBase : Controller
    {
        public ControllerBase()
        {
        }

        public ControllerBase(IWebshopBus Bus)
        {
            WebshopBus = Bus;
        }

        protected virtual IWebshopBus WebshopBus
        {
            get;
            set;
        }

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding)
        {
            return new JsonDotNetResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding
            };
        }

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonDotNetResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding
            };
        }
    }
}