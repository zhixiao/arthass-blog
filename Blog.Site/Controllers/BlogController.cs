﻿using Blog.Site.Constants;
using Blog.Site.Helper;
using Blog.Site.Models;
using Arthas.Blogs.Core;
using Arthas.Blogs.Core.Commands;
using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Site.Controllers
{
    public class BlogController : ControllerBase
    {
        readonly IDataQuerier DataQuerier;

        public BlogController(IDataQuerier dataQuerier,IWebshopBus Bus)
        {
            DataQuerier = dataQuerier;
            WebshopBus = Bus;
        }

        public ActionResult Index(int CategoryID = 0)
        {
            var spec = BlogSpec.IsNotDelete();

            if (CategoryID != 0)
            {
                spec = spec.And(BlogSpec.IsCategory(CategoryID));
            }

            var query = DataQuerier.GetQuery(spec);

            var urlBase = Url.Action("Search", "Blog");
            var testImage = Url.Content("~/Content/images/img-post.jpg");
            var permalinkBase = Url.Action("Detail", "Blog");

            var viewModel = query.OrderByDescending(it => it.Status).ThenByDescending(it => it.CreateDate).ToList().Select(it => new BlogItem()
            {
                ID = it.ID,
                Title = it.Title,
                TagString = it.Tags,
                TagUrlBase = urlBase,
                CreateDateTime = it.CreateDate,
                Description = it.Description.Replace(BlogConstants.BLOG_RELATIVE_PATH, BlogConstants.BLOG_ADMIN_SITE_DOMAIN),
                HomepageImage = testImage,
                Author = "Admin",
                RepliesCount = it.Reviews.Where(item => item.IsDeleted == false).Count(),
                PermaLink = permalinkBase + "/" + it.ID.ToString()
            }).ToList();

            ViewBag.Test = EncryptConnectionString.GetProtectSection("connectionStrings");

            return View(viewModel);
        }

        public ActionResult Detail(int id)
        {
            var blog = DataQuerier.GetByID<Arthas.Blogs.Core.Model.Blog, int>(id);

            var viewModel = AutoMapper.Mapper.Map<Arthas.Blogs.Core.Model.Blog, DetailModel>(blog);

            viewModel.HomepageImage = Url.Content("~/Content/images/img-post.jpg");
            viewModel.PermaLink = Url.Action("Detail", "Blog", new { id = id });
            viewModel.TagUrlBase = Url.Action("Search", "Blog");
            viewModel.Author = "Admin";

            return View(viewModel);
        }

        [HttpPost]
        public JsonResult CreateComment(CreateComment model)
        {
            try
            {
                WebshopBus.Send(new CreateCommentCommand(BlogSiteContext.CurrentUser.UserID, model.text, model.parent_id, model.blogID));

                var review = DataQuerier.GetByID<Review,int>(model.parent_id);

                return Json(new
                {
                    success = true,
                    posted_date = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
                    text = model.text,
                    childrens = new List<string>(),
                    parent_id = model.parent_id,
                    in_reply_to = review != null ? review.Reviewer.UserName : ""
                });
            }
            catch (Exception ex)
            {
                return Json(new { success = false });
            }
        }

        [HttpPost]
        public JsonResult EditComment(int id, CreateComment model)
        {
            try
            {
                WebshopBus.Send(new EditCommentCommand(id, model.text));

                return Json(new
                {
                    success = true,
                    posted_date = DateTime.UtcNow.ToString("yyyy-MM-dd hh:mm:ss"),
                    text = model.text,
                    childrens = new List<string>(),
                    parent_id = model.parent_id
                });
            }
            catch (Exception ex)
            {
                return Json(new { success = false });
            }
        }

        [HttpPost]
        public JsonResult DeleteComment(int comment_id)
        {
            try
            {
                WebshopBus.Send(new DeleteCommentCommand(comment_id));
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false });
            }
        }
    }
}
