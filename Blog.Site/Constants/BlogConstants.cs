﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Site.Constants
{
    public class BlogConstants
    {
        public const string BLOG_ADMIN_SITE_DOMAIN = "http://arthasadmin.apphb.com/";

        public const string BLOG_RELATIVE_PATH = "../../";
    }
}