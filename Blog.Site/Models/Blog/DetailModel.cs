﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Site.Models
{
    public class DetailModel : BlogItem
    {
        public string Content { get; set; }
    }
}