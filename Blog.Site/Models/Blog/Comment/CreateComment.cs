﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Site.Models
{
    public class CreateComment
    {
        public int parent_id { get; set; }

        public string text { get; set; }

        public int blogID { get; set; }
    }
}