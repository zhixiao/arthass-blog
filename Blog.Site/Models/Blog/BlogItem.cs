﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Site.Models
{
    public class BlogItem : ModelBase
    {
        public string Title { get; set; }

        public string TagString { get; set; }

        public string TagUrlBase { get; set; }

        public List<TagItem> Tags 
        { 
            get 
            {
                var tagList = new List<TagItem>();

                if (string.IsNullOrEmpty(TagString) == false)
                {
                    var tags = TagString.Split(new string[]{ "," }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var item in tags)
                    {
                        tagList.Add(new TagItem() { Name = item, Url = TagUrlBase + "?keyword=" + item  });
                    }
                }

                return tagList;
            }
        }

        public string HomepageImage { get; set; }

        public string Description { get; set; }

        public DateTime CreateDateTime { get; set; }

        public string CreateMonth 
        {
            get 
            {
                return CreateDateTime.ToString("MMM");
            }
        }

        public string CreateDate
        {
            get
            {
                return CreateDateTime.ToString("dd");
            }
        }

        public string CreateTime 
        {
            get
            {
                return CreateDateTime.ToString("HH:mm");
            }
        }

        public string Author { get; set; }

        public int RepliesCount { get; set; }

        public string PermaLink { get; set; }
    }

    public class TagItem
    {
        public string Name { get; set; }

        public string Url { get; set; }
    }
}