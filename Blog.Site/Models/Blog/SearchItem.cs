﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Site.Models
{
    public class SearchItem : ModelBase
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string CreateDate { get; set; }

        public string TagString { get; set; }

        public string TagUrlBase { get; set; }

        public List<TagItem> Tags
        {
            get
            {
                var tagList = new List<TagItem>();

                if (string.IsNullOrEmpty(TagString) == false)
                {
                    var tags = TagString.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var item in tags)
                    {
                        tagList.Add(new TagItem() { Name = item, Url = TagUrlBase + "?keyword=" + item });
                    }
                }

                return tagList;
            }
        }
    }
}