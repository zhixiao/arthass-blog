﻿using Arthas.Blogs.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Blog.Site.Models
{
    public class ReviewItem
    {
        public int comment_id { get; set; }

        public int parent_id { get; set; }

        public string in_reply_to { get; set; }

        [ScriptIgnore]
        public Review VirtualReview { get; set; }

        public int element_id 
        {
            get
            {
                var _review = VirtualReview;
                while (_review.Parent != null)
                {
                    _review = _review.Parent;
                }
                return _review.ID;
            }
        }

        public int created_by { get; set; }

        public string fullname { get; set; }

        public string picture { get; set; }

        public string posted_date { get; set; }

        public string text { get; set; }

        public List<string> attachments { get; set; }

        public List<ReviewItem> childrens { get; set; }
    }
}