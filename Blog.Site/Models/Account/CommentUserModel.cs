﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Site.Models
{
    public class CommentUserModel
    {
        public CommentUserModel()
        {
            fullname = "Guest";
            is_logged_in = false;
            is_add_allowed = false;
            is_edit_allowed = false;
        }

        public int user_id { get; set; }

        public string fullname { get; set; }

        public string picture { get; set; }

        public bool is_logged_in { get; set; }

        public bool is_add_allowed { get; set; }

        public bool is_edit_allowed { get; set; }
    }
}