﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Site.Models
{
    public class RegisterUserModel
    {
        [Required]
        [System.Web.Mvc.Remote("VerifyUserName", "Account", ErrorMessage = "UserName is already exist.")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [System.Web.Mvc.Remote("VerifyEmail", "Account", ErrorMessage = "Email is already exist.")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(int.MaxValue, MinimumLength = 6, ErrorMessage = "Password cannot be less than 6 characters.")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Two password must be same.")]
        public string ConfirmPassword { get; set; }

        public string ProfileImage { get; set; }
    }
}