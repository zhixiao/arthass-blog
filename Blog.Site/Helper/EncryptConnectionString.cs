﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Blog.Site.Helper
{
    public class EncryptConnectionString
    {
        public static void ProtectSection(string strSectionName, string provider = "DataProtectionConfigurationProvider")
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~");

            ConfigurationSection section = config.GetSection(strSectionName);

            if (section != null && !section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection(provider);
                config.Save();
            }
        }



        public static string GetProtectSection(string strSectionName)
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
            ConfigurationSection section = config.GetSection(strSectionName);

            if (section != null)
            {
                section.SectionInformation.UnprotectSection();
                ConnectionStringSettings connectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationConnection"];
                return connectionStringSettings.ConnectionString;
            }

            return string.Empty;
        }
    }
}