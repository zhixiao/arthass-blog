﻿using Arthas.Blogs.Infrastructure.Tool.Ioc;
using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Core.Queries.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Blog.Site.Helper
{
    public class BlogSiteContext
    {
        public static CurrentUserModel CurrentUser
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                {
                    return null;
                }

                var userData = ((System.Web.Security.FormsIdentity)HttpContext.Current.User.Identity).Ticket.UserData;
                var DeserializeUserData = (new System.Web.Script.Serialization.JavaScriptSerializer()).Deserialize<CurrentUserModel>(userData);

                return DeserializeUserData;
            }
        }

        public static void SetAuthTicketCookie(int UserID, string UserName, bool RememberMe)
        {
            int timeout = 60 * 2;
            if (RememberMe)
            {
                timeout = 60 * 24 * 3;
            }

            var cookieUser = new CurrentUserModel() 
            {
                UserID = UserID,
                UserName = UserName
            };

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var ticket = new FormsAuthenticationTicket(
                0,
                UserID.ToString(), 
                DateTime.UtcNow, 
                DateTime.UtcNow.AddMinutes(timeout), 
                false,
                serializer.Serialize(cookieUser));

            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket)) { HttpOnly = true };
            if (RememberMe)
                authCookie.Expires = DateTime.UtcNow.AddMinutes(timeout);
            HttpContext.Current.Response.Cookies.Add(authCookie);
        }

        public static ProfileInfo ProfileInfo 
        {
            get
            {
                var dataQuerier = Container.Current.Resolve<IDataQuerier>();
                var profile = dataQuerier.GetQuery(AdministratorSpec.HasID(1)).Select(it => new ProfileInfo { ProfileImage = it.ProfileImage, AboutMe = it.AboutMe }).FirstOrDefault();
                return profile;
            }
        }

        public static List<string> Tags
        {
            get
            {
                var DataQuerier = Container.Current.Resolve<IDataQuerier>();
                var tagStrings = DataQuerier.GetQuery(BlogSpec.IsNotDelete()).Select(it => it.Tags).ToList();
                return GetTags(tagStrings);
            }
        }

        public static List<SideBarBlog> MostpopularBlog
        {
            get
            {
                var DataQuerier = Container.Current.Resolve<IDataQuerier>();
                var MostPopular = DataQuerier.GetQuery(BlogSpec.IsNotDelete()).OrderByDescending(it => it.ReadCount).Take(5).ToList().Select(it =>
                new SideBarBlog { ID = it.ID, Title = it.Title, PostDate = it.CreateDate.ToString("MMMM dd,yyyy") }).ToList();

                return MostPopular;
            }
        }

        public static List<SideBarComment> RecentlyComments
        {
            get
            {
                var DataQuerier = Container.Current.Resolve<IDataQuerier>();
                var RecentlyComment = DataQuerier.GetQuery(ReviewSpec.IsNotDelete<Review>()).OrderByDescending(it => it.CreateDateTime).Take(5).ToList()
               .Select(it => new SideBarComment
               {
                   ID = it.ID,
                   Thumbnail = it.Content.Length > 20 ? it.Content.Substring(0, 20) : it.Content,
                   Author = it.Reviewer.UserName
               }).ToList();

                return RecentlyComment;
            }
        }

        public static List<TopCategory> Categorys
        {
            get
            {
                var DataQuerier = Container.Current.Resolve<IDataQuerier>();
                return DataQuerier.GetQuery(CategorySpec.IsNotDelete<Category>()).OrderBy(it => it.Sequence).Select(it => 
                    new TopCategory()
                    {
                        ID = it.ID,
                        Title = it.CategoryName
                    }).ToList();
            }
        }

        private static List<string> GetTags(List<string> tagstrings)
        {
            var tags = new List<string>();

            foreach (var str in tagstrings)
            {
                var _tags = str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var _tag in _tags)
                {
                    tags.Add(_tag);
                }
            }

            return tags;
        }
    }

    public class CurrentUserModel
    {
        public int UserID { get; set; }

        public string UserName { get; set; }
    }

    public class ProfileInfo
    {
        public string ProfileImage { get; set; }

        public string AboutMe { get; set; }
    }

    public class SideBarBlog
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string PostDate { get; set; }
    }

    public class SideBarComment
    {
        public int ID { get; set; }

        public string Thumbnail { get; set; }

        public string Author { get; set; }
    }

    public class TopCategory
    {
        public int ID { get; set; }

        public string Title { get; set; }
    }
}