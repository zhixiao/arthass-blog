﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Blog.Site.Models;

namespace Blog.Site.App_Start
{
    public static class AutoMapperConfig
    {
        public static void MapModels()
        {
            Mapper.CreateMap<Arthas.Blogs.Core.Model.Blog, DetailModel>()
                .ForMember(d => d.CreateDateTime, opt => opt.MapFrom(source => source.CreateDate))
                .ForMember(d => d.TagString, opt => opt.MapFrom(source => source.Tags))
                .ForMember(d => d.RepliesCount, opt => opt.MapFrom(source => source.Reviews.Where(it => it.IsDeleted == false).Count()))
                .ForMember(d => d.Tags,opt =>opt.Ignore())
                .IgnoreAllNoneExisting();
        }
    }

    static class AutoMapperExtension
    {
        public static IMappingExpression<TSource, TDestination>
            IgnoreAllNoneExisting<TSource, TDestination>(this IMappingExpression<TSource, TDestination> expression)
        {
            var sourceType = typeof(TSource);
            var destinationType = typeof(TDestination);
            var existingMaps = Mapper.GetAllTypeMaps().First(x => x.SourceType.Equals(sourceType) && x.DestinationType.Equals(destinationType));
            foreach (var property in existingMaps.GetUnmappedPropertyNames())
            {
                expression.ForMember(property, opt => opt.Ignore());
            }
            return expression;
        }
    }
}