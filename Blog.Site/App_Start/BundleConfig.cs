﻿using System.Web;
using System.Web.Optimization;

namespace Blog.Site
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery-migrate-*",
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                        "~/Scripts/knockout-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquerycomment").Include(
                        "~/Scripts/Jquery-Comments/js/jquery.timeago.js",
                        "~/Scripts/Jquery-Comments/js/jquery.autogrow-textarea.js",
                        "~/Scripts/Jquery-Comments/js/jquery.comment.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryfileupload").Include(
                       "~/Scripts/jquery.ui.widget.js",
                       "~/Scripts/jquery.fileupload.js",
                       "~/Scripts/jquery.iframe-transport.js"));

            bundles.Add(new ScriptBundle("~/bundles/syntaxhighlighter").Include(
                "~/Scripts/syntaxhighlighter/src/shCore.js",
                 "~/Scripts/syntaxhighlighter/scripts/shBrushCSharp.js",
                 "~/Scripts/syntaxhighlighter/scripts/shBrushJScript.js",
                 "~/Scripts/syntaxhighlighter/src/Init.js"
                ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css","~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/CoolBlue").Include(
                "~/Content/Coolblue/coolblue.css", "~/Content/Coolblue/reset.css", "~/Content/Coolblue/enhancements.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Content/jquerycomment").Include(
                        "~/Scripts/jquery.ui.css",
                        "~/Scripts/Jquery-Comments/css/jquery.comment.css"));

            bundles.Add(new StyleBundle("~/Content/syntaxhighlighter").Include(
                    "~/Scripts/syntaxhighlighter/styles/shCore.css",
                    "~/Scripts/syntaxhighlighter/styles/shThemeDefault.css"
                ));
        }
    }
}