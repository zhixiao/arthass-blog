﻿using Arthas.Blogs.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Data
{
    public partial class ApplicationContext
    {
        public DbSet<Photo> Photos { get; set; }
    }
}
