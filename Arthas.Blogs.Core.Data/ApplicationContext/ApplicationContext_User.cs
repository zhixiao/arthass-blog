﻿using System.Data.Entity;
using Arthas.Blogs.Core.Model;

namespace Arthas.Blogs.Core.Data
{
    public partial class ApplicationContext
    {
        public DbSet<User> Users { get; set; }

        private void MapUser(DbModelBuilder modelBuilder)
        {
            var userEntity = modelBuilder.Entity<User>();
            userEntity.HasMany(user => user.LoginLogs);
        }
    }
}
