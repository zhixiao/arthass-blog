﻿using System.Data.Entity;

namespace Arthas.Blogs.Core.Data
{
    public partial class ApplicationContext : DbContext
    {
        public ApplicationContext()
            : base("ApplicationConnection")
        { 
            
        }
        public ApplicationContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        { 
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            MapUser(modelBuilder);
            MapBlogs(modelBuilder);
            MapCategorys(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }
    }
}
