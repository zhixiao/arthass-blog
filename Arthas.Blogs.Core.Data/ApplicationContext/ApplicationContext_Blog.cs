﻿using Arthas.Blogs.Core.Model;
using System.Data.Entity;

namespace Arthas.Blogs.Core.Data
{
    public partial class ApplicationContext
    {
        DbSet<Blog> Blogs { get; set; }

        private void MapBlogs(DbModelBuilder modelBuilder)
        {
            var blogEntity = modelBuilder.Entity<Blog>();
            blogEntity.HasMany(blog => blog.Reviews);
        }
    }
}
