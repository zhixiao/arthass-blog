﻿using Arthas.Blogs.Core.Model;
using System.Data.Entity;

namespace Arthas.Blogs.Core.Data
{
    public partial class ApplicationContext
    {
        DbSet<Review> Reviews { get; set; }
    }
}
