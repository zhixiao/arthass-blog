﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Data
{
    using Arthas.Blogs.Infrastructure.Tool;
    using Arthas.Blogs.Infrastructure.Tool.Ioc;
    using Arthas.Blogs.Core.Data.Queries;
    using Arthas.Blogs.Core.Data.Repositories;
    using Arthas.Blogs.Core.Queries;
    using Arthas.Blogs.Core.Repositories;

    public class RegisterCoreDataStartupTask : IStartupTask
    {
        public void Execute()
        {
            Container.Current.Register<ApplicationContext>();
            Container.Current.Register<IDataContext, DataContext>();
            Container.Current.Register<IDataQuerier, DataQuerier>(false);
        }

        public int Order
        {
            get
            {
                return 0;
            }
        }
    }
}
