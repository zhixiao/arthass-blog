namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Blog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Blogs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Content = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        LastUpdate = c.DateTime(nullable: false),
                        DateCategory = c.String(),
                        ApprovalCount = c.Int(nullable: false),
                        ReadCount = c.Int(nullable: false),
                        IsAlwaysTop = c.Boolean(nullable: false),
                        Tags = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Category_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Categories", t => t.Category_ID)
                .Index(t => t.Category_ID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        Sequence = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Content = c.String(),
                        CreateDateTime = c.DateTime(nullable: false),
                        AgreeCount = c.Int(nullable: false),
                        ReplyCount = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Reviewer_ID = c.Int(),
                        Blog_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.Reviewer_ID)
                .ForeignKey("dbo.Blogs", t => t.Blog_ID)
                .Index(t => t.Reviewer_ID)
                .Index(t => t.Blog_ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Reviews", new[] { "Blog_ID" });
            DropIndex("dbo.Reviews", new[] { "Reviewer_ID" });
            DropIndex("dbo.Blogs", new[] { "Category_ID" });
            DropForeignKey("dbo.Reviews", "Blog_ID", "dbo.Blogs");
            DropForeignKey("dbo.Reviews", "Reviewer_ID", "dbo.Users");
            DropForeignKey("dbo.Blogs", "Category_ID", "dbo.Categories");
            DropTable("dbo.Reviews");
            DropTable("dbo.Categories");
            DropTable("dbo.Blogs");
        }
    }
}
