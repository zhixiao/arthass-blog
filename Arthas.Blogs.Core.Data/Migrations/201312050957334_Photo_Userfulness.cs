namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Photo_Userfulness : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Photos", "Usefulness", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Photos", "Usefulness");
        }
    }
}
