namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Approachs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApproachUsers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Approach = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        User_ID = c.Int(),
                        Review_ID = c.Int(),
                        Blog_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .ForeignKey("dbo.Reviews", t => t.Review_ID)
                .ForeignKey("dbo.Blogs", t => t.Blog_ID)
                .Index(t => t.User_ID)
                .Index(t => t.Review_ID)
                .Index(t => t.Blog_ID);
            
            AddColumn("dbo.Users", "ProfileImage", c => c.String());
            DropColumn("dbo.Reviews", "AgreeCount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reviews", "AgreeCount", c => c.Int(nullable: false));
            DropIndex("dbo.ApproachUsers", new[] { "Blog_ID" });
            DropIndex("dbo.ApproachUsers", new[] { "Review_ID" });
            DropIndex("dbo.ApproachUsers", new[] { "User_ID" });
            DropForeignKey("dbo.ApproachUsers", "Blog_ID", "dbo.Blogs");
            DropForeignKey("dbo.ApproachUsers", "Review_ID", "dbo.Reviews");
            DropForeignKey("dbo.ApproachUsers", "User_ID", "dbo.Users");
            DropColumn("dbo.Users", "ProfileImage");
            DropTable("dbo.ApproachUsers");
        }
    }
}
