namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Review_Title : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Reviews", "Title");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reviews", "Title", c => c.String());
        }
    }
}
