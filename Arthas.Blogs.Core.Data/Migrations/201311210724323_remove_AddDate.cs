namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_AddDate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Photos", "AddDate");
            DropColumn("dbo.Users", "AddDate");
            DropColumn("dbo.LoginLogs", "AddDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LoginLogs", "AddDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Users", "AddDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Photos", "AddDate", c => c.DateTime(nullable: false));
        }
    }
}
