namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductDetailImages", "Product_ID", "dbo.Products");
            DropIndex("dbo.ProductDetailImages", new[] { "Product_ID" });
            DropTable("dbo.Products");
            DropTable("dbo.ProductDetailImages");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProductDetailImages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Label = c.String(),
                        Thumbnail = c.String(),
                        LargeImage = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Product_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ShortDescription = c.String(),
                        Description = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OverviewImage = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.ProductDetailImages", "Product_ID");
            AddForeignKey("dbo.ProductDetailImages", "Product_ID", "dbo.Products", "ID", cascadeDelete: true);
        }
    }
}
