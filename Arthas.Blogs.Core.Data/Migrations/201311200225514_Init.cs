namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 20),
                        Password = c.String(nullable: false, maxLength: 32),
                        Email = c.String(nullable: false, maxLength: 50),
                        IsActived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LoginLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IPAddress = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        AddDate = c.DateTime(nullable: false),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.User_ID);
        }
        
        public override void Down()
        {
            DropIndex("dbo.LoginLogs", new[] { "User_ID" });
            DropForeignKey("dbo.LoginLogs", "User_ID", "dbo.Users");
            DropTable("dbo.LoginLogs");
            DropTable("dbo.Users");
        }
    }
}
