namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create_Administrator : DbMigration
    {

        public override void Up()
        {
            CreateTable(
             "dbo.Administrators",
             c => new
             {
                 ID = c.Int(nullable: false, identity: true),
                 UserName = c.String(),
                 PassWord = c.String(),
                 IsActived = c.Boolean(nullable: true),
                 IsDeleted = c.Boolean(nullable: false),
                 CreateDate = c.DateTime(nullable: false),
             })
             .PrimaryKey(t => t.ID);
        }

        public override void Down()
        {
            DropTable("dbo.Administrators");
        }

    }
}
