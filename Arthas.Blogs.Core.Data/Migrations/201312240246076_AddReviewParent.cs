namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReviewParent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reviews", "Parent_ID", c => c.Int());
            AddForeignKey("dbo.Reviews", "Parent_ID", "dbo.Reviews", "ID");
            CreateIndex("dbo.Reviews", "Parent_ID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Reviews", new[] { "Parent_ID" });
            DropForeignKey("dbo.Reviews", "Parent_ID", "dbo.Reviews");
            DropColumn("dbo.Reviews", "Parent_ID");
        }
    }
}
