namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Administrator_1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Administrators", "AboutMe", c => c.String());
            AddColumn("dbo.Administrators", "ProfileImage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Administrators", "ProfileImage");
            DropColumn("dbo.Administrators", "AboutMe");
        }
    }
}
