namespace Arthas.Blogs.Core.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BlogStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "Status", c => c.Int(nullable: false));
            DropColumn("dbo.Blogs", "IsAlwaysTop");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Blogs", "IsAlwaysTop", c => c.Boolean(nullable: false));
            DropColumn("dbo.Blogs", "Status");
        }
    }
}
