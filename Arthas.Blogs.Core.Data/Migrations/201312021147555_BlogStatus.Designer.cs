// <auto-generated />
namespace Arthas.Blogs.Core.Data.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class BlogStatus : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(BlogStatus));
        
        string IMigrationMetadata.Id
        {
            get { return "201312021147555_BlogStatus"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
