﻿using Arthas.Blogs.Infrastructure.Data;
using Arthas.Blogs.Infrastructure.Data.Repository;

namespace Arthas.Blogs.Core.Data
{
    public class RepositoryBase<TEntity,TKey> : Repository<TEntity,TKey>
        where TEntity : EntityBase<TKey>
    {
        public RepositoryBase(ApplicationContext context)
            :base(context)
        {
            DbContext = context;
        }

        protected new virtual ApplicationContext DbContext { get; set; }
    }
}
