﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Arthas.Blogs.Core.Queries;
using Arthas.Blogs.Infrastructure.Data;

namespace Arthas.Blogs.Core.Data.Queries
{
    public class DataQuerier : IDataQuerier
    {
        readonly ApplicationContext AppContext;
        public DataQuerier()
        {
            AppContext = new ApplicationContext();
        }

        public IQueryable<T> GetQuery<T>(ISpecification<T> specification) where T : class, IEntity
        {
            IQueryable<T> query = null;
            if (specification == null)
            {
                query = AppContext.Set<T>().AsQueryable();
            }
            else 
            {
                query = AppContext.Set<T>().Where(specification.Predicate);
            }

            return query;
        }

        public T GetOne<T>(ISpecification<T> specification = null) where T : class, IEntity
        {
            return GetQuery(specification).FirstOrDefault();
        }

        public T GetByID<T, K>(K id) where T : class, IEntity
        {
            return AppContext.Set<T>().Find(id);
        }

        public void Refresh<T>(T entry) where T : class, IEntity
        {
            AppContext.Entry(entry).Reload();
        }
    }
}
