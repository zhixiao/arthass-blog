﻿using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Repositories;

namespace Arthas.Blogs.Core.Data.Repositories
{
    public class CategoryRepository : RepositoryBase<Category,int> , ICategoryRepository
    {
        public CategoryRepository(ApplicationContext context)
            : base(context)
        { 
        
        }    
    }
}
