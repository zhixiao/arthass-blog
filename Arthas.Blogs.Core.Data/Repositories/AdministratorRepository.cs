﻿using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Repositories;

namespace Arthas.Blogs.Core.Data.Repositories
{
    public class AdministratorRepository : RepositoryBase<Administrator,int>,IAdministratorRepository
    {
        public AdministratorRepository(ApplicationContext context)
            : base(context)
        { 
            
        }
    }
}
