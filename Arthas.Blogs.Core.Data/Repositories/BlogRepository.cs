﻿using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Repositories;

namespace Arthas.Blogs.Core.Data.Repositories
{
    public class BlogRepository : RepositoryBase<Blog, int>, IBlogRepository
    {
        public BlogRepository(ApplicationContext context)
            : base(context)
        { 
            
        }
    }
}
