﻿using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Repositories;


namespace Arthas.Blogs.Core.Data.Repositories
{
    public class PhotoRepository : RepositoryBase<Photo, int>, IPhotoRepository
    {
        public PhotoRepository(ApplicationContext context)
            : base(context)
        { 
        }
    }
}
