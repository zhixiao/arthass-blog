﻿using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Repositories;

namespace Arthas.Blogs.Core.Data.Repositories
{
    public class UserRepository : RepositoryBase<User,int>,IUserRepository
    {
        public UserRepository(ApplicationContext context)
            : base(context)
        { 
        }
    }
}
