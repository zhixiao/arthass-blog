﻿using Arthas.Blogs.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Core.Data.Repositories
{
    public class DataContext : IDataContext
    {
        public event Action OnCommitted;

        ApplicationContext ApplicationContext;
        public DataContext(ApplicationContext Context)
        {
            ApplicationContext = Context;
        }

        public void Commit()
        {
            ApplicationContext.SaveChanges();
        }

        public void Dispose()
        {
            ApplicationContext.Dispose();
        }

        private IAdministratorRepository _AdministratorRepository;
        public IAdministratorRepository AdministratorRepository
        {
            get { return _AdministratorRepository ?? (_AdministratorRepository = new AdministratorRepository(ApplicationContext)); }
        }

        private ICategoryRepository _CategoryRepository;
        public ICategoryRepository CategoryRepository
        {
            get { return _CategoryRepository ?? (_CategoryRepository = new CategoryRepository(ApplicationContext)); }
        }

        private IBlogRepository _BlogRepository;
        public IBlogRepository BlogRepository
        {
            get { return _BlogRepository ?? (_BlogRepository = new BlogRepository(ApplicationContext)); }
        }

        private IPhotoRepository _PhotoRepository;
        public IPhotoRepository PhotoRepository
        {
            get { return _PhotoRepository ?? (_PhotoRepository = new PhotoRepository(ApplicationContext)); }
        }

        private IUserRepository _UserRepository;
        public IUserRepository UserRepository
        {
            get { return _UserRepository ?? (_UserRepository = new UserRepository(ApplicationContext)); }
        }

        private ICommentRepository _CommentRepository;
        public ICommentRepository CommentRepository
        {
            get { return _CommentRepository ?? (_CommentRepository = new CommentRepository(ApplicationContext)); }
        }
    }
}
