﻿using Arthas.Blogs.Core.Model;
using Arthas.Blogs.Core.Repositories;

namespace Arthas.Blogs.Core.Data.Repositories 
{
    public class CommentRepository : RepositoryBase<Review, int> , ICommentRepository
    {
        public CommentRepository(ApplicationContext context)
            : base(context)
        { 
        }
    }
}
