﻿try {
    if (!window.CloudFlare) {
        var CloudFlare =
            [
                {
                    verbose: 0,
                    p: 0,
                    byc: 0,
                    owlid: "cf",
                    bag2: 1,
                    mirage2: 0,
                    oracle: 0,
                    paths: { cloudflare: "/cdn-cgi/nexp/acv=2980380653/" },
                    atok: "b21d030524e250a0a28fa04f7f9c0448",
                    petok: "d1b968b4f8d37af2794344d57a4c2af3-1384652740-1800",
                    zone: "template-help.com",
                    rocket: "0",
                    apps: {
                        "abetterbrowser": { "ie": "7" },
                        "ga_key": { "ua": "UA-7078796-5", "ga_bs": "2" }
                    }
                }
            ];
        var a = document.createElement("script"),
            b = document.getElementsByTagName("script")[0];
        a.async = !0;
        a.src = "//ajax.cloudflare.com/cdn-cgi/nexp/acv=616370820/cloudflare.min.js";
        b.parentNode.insertBefore(a, b);
    }
} catch (e) { };