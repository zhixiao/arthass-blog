﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Arthas.Blogs.Infrastructure.Tool.Ioc
{
    public class Container
    {
        protected IContainer AutofacContainer
        {
            get;
            private set;
        }

        protected IContainer AutofacContainerAsync
        {
            get;
            private set;
        }

        protected Container()
        {
            AutofacContainer = new ContainerBuilder().Build();
            AutofacContainerAsync = new ContainerBuilder().Build();
        }

        static Container _container;
        public static Container Current
        {
            get
            {
                if (_container == null)
                {
                    _container = new Container();
                }

                return _container;
            }
        }

        public void Register<T>(bool perHttpRequest = true)
        {
            var updater = new ContainerBuilder();

            var registrationBuilder = updater.RegisterType<T>();
            if (perHttpRequest)
            {
                registrationBuilder.InstancePerHttpRequest().InstancePerApiRequest();
            }

            updater.Update(AutofacContainer);

            var updateAsync = new ContainerBuilder();
            updateAsync.RegisterType<T>();
            updateAsync.Update(AutofacContainerAsync);
        }

        public void Register<TService, TImplementer>(bool perHttpRequest = true)
        {
            var updater = new ContainerBuilder();

            var registrationBuilder = updater.RegisterType<TImplementer>().As<TService>();
            if (perHttpRequest)
            {
                registrationBuilder.InstancePerHttpRequest().InstancePerApiRequest();
            }

            updater.Update(AutofacContainer);

            var updateAsync = new ContainerBuilder();
            updateAsync.RegisterType<TImplementer>().As<TService>();
            updateAsync.Update(AutofacContainerAsync);
        }

        public T Resolve<T>() where T : class
        {
            if (HttpContext.Current == null)
            {
                return AutofacContainer.Resolve<T>();
            }

            if (HttpContext.Current.Items["WebAPIIocScope"] == null)
            {
                return System.Web.Mvc.DependencyResolver.Current.GetService<T>();
            }
            else
            {
                var scrop = HttpContext.Current.Items["WebAPIIocScope"] as Autofac.Integration.WebApi.AutofacWebApiDependencyScope;
                return scrop.GetService(typeof(T)) as T;
            }
        }

        public T ResolveAsync<T>() where T : class
        {
            return AutofacContainerAsync.Resolve<T>();
        }

        public void RegisterController()
        {
            var build = new ContainerBuilder();

            build.RegisterControllers(GetApplicationAssembly());

            build.Update(AutofacContainer);
        }

        public void RegisterApiController()
        {
            var build = new ContainerBuilder();

            build.RegisterApiControllers(GetApplicationAssembly());

            build.Update(AutofacContainer);
        }

        private static Assembly GetApplicationAssembly()
        {
            Assembly assembly = Assembly.GetEntryAssembly();

            HttpContext ctx = HttpContext.Current;
            if (ctx != null)
            {
                assembly = GetWebApplicationAssembly(ctx);
            }

            return assembly ?? (Assembly.GetExecutingAssembly());
        }

        private static Assembly GetWebApplicationAssembly(HttpContext context)
        {
            object app = context.ApplicationInstance;
            if (app == null)
            {
                return null;
            }

            Type type = app.GetType();
            while (type != null && type != typeof(object) && type.Namespace == "ASP")
            {
                type = type.BaseType;
            }

            return type.Assembly;
        }

        internal IContainer GetAutofacContainer()
        {
            return AutofacContainer;
        }
    }
}
