﻿using Arthas.Blogs.Infrastructure.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Arthas.Blogs.Infrastructure.Tool.Applications
{
    public class ApplicationBase : Application
    {
        protected void Application_Start()
        {
            new StartupTaskHelper().RunStartupTask();

            var container = Container.Current;
            //container.RegisterController();
            //container.RegisterApiController();
        }
    }
}
