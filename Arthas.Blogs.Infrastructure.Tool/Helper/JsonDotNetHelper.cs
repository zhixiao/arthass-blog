﻿using Newtonsoft.Json;
using System;

namespace Arthas.Blogs.Infrastructure.Tool.Helper
{
    public class JsonDotNetHelper
    {
        public static string ConvertToJson(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static string EnumConvertToJson(Type e)
        {
            var ret = "[";
            foreach (var val in Enum.GetValues(e))
            {
                var name = Enum.GetName(e, val);
                ret += "{\"label\":\"" + name + "\"," + "\"value\":" + ((int)val).ToString() + "},";
            }
            ret += "]";
            return ret.Replace(",]", "]"); 
        }
    }
}
