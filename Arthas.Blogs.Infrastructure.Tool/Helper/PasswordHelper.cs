﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.Helper
{
    public class PasswordHelper
    {
        public static string HashPassword(string password)
        {
            var sha1 = new System.Security.Cryptography.SHA1Managed();
            var src = System.Text.Encoding.ASCII.GetBytes(password);
            var dest = sha1.ComputeHash(src);
            return Convert.ToBase64String(dest);
        }
    }
}
