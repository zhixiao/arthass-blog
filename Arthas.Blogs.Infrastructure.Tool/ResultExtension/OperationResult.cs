﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool
{
    public class OperationResult
    {
        public OperationResult(OperationResultType type)
        {
            Type = type;
        }

        public OperationResult(OperationResultType type,string message)
            :this(type)
        {
            Message = message;
        }

        public OperationResult(OperationResultType type,string message,string logMessage)
            :this(type,message)
        {
            LogMessage = logMessage;
        }

        public OperationResult(OperationResultType type, string message, object appendData)
            :this(type,message)
        {
            AppendData = appendData;
        }

        public OperationResult(OperationResultType type, string message, string logMessage, object appendData)
            :this(type,message,logMessage)
        {
            AppendData = appendData;
        }


        #region Field

        public OperationResultType Type { get; set; }

        public string Message { get; set; }

        public string LogMessage { get; set; }

        public object AppendData { get; set; }

        #endregion
    }

    [Description("业务操作结果")]
    public enum OperationResultType
    { 
        [Description("操作成功")]
        Success,

        [Description("操作没有引发任何变化，提交取消")]
        Nochange,

        [Description("参数错误")]
        ParamterError,

        [Description("查询为空")]
        QueryNull,

        [Description("缺乏相关权限")]
        PurviewLack,

        [Description("非法操作")]
        IlligalOperation,

        [Description("警告")]
        Warning,

        [Description("操作引发异常")]
        Error
    }
}
