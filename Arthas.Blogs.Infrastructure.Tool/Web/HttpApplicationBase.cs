﻿using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Arthas.Blogs.Infrastructure.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.Web
{
    public abstract class HttpApplicationBase : System.Web.HttpApplication
    {
        protected virtual bool IsWebAPIProject
        {
            get
            {
                return false;
            }
        }

        protected void Application_Start()
        {

            // Run all start up tasks
            new WebStartupTaskHelper().RunStartupTask();

            // Register Web Api controllers
            var container = Container.Current;
            container.RegisterController();
            container.RegisterApiController();

            System.Web.Mvc.DependencyResolver.SetResolver(new DependencyResolver(container));

            InfrastructureApplicationStart();
        }

        /// <summary>
        /// Instead of the default Application_Start
        /// </summary>
        public abstract void InfrastructureApplicationStart();
    }

    public class DependencyResolver : AutofacDependencyResolver
    {
        public DependencyResolver()
            : this(Container.Current)
        {
        }

        public DependencyResolver(Container container)
            : base(container.GetAutofacContainer())
        {
        }
    } 
}
