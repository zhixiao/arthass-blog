﻿using Arthas.Blogs.Infrastructure.Tool.Web.TypeFinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.Web
{
    public class WebStartupTaskHelper : StartupTaskHelper
    {
        public override ITypeFinder TypeFinder
        {
            get
            {
                return new WebAppTypeFinder();
            }
        }
    }
}
