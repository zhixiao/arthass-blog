﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kooboo.Project.Component.Tool.Linq.Dynamic;

namespace Kooboo.Project.Component.Tool.Web.Sorting
{
    public static class QueryableExtension
    {
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> queryable, string sortBy, string sortDirection)
        {
            if (String.IsNullOrEmpty(sortDirection))
            {
                return queryable.OrderBy(sortBy);
            }

            return queryable.OrderBy(sortBy + " " + sortDirection);
        }
    }
}
