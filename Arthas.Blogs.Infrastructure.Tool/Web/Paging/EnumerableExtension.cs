﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.Web.Paging
{
    public static class EnumerableExtension
    {
        private const int DefaultPageIndex = 1;
        private const int DefaultPageSize = 20;

        public static PagedList<T> ToPageList<T>(this IEnumerable<T> allItems)
        {
            return ToPageList(allItems, DefaultPageIndex, DefaultPageSize);
        }

        public static PagedList<T> ToPageList<T>(this IEnumerable<T> allItems, int? pageIndex, int? pageSize)
        {
            return ToPageList(allItems, pageIndex.HasValue ? pageIndex.Value : DefaultPageIndex, pageSize);
        }

        public static PagedList<T> ToPageList<T>(this IEnumerable<T> allItems, int pageIndex, int? pageSize)
        {
            return ToPageList(allItems, pageIndex, pageSize.HasValue ? pageSize.Value : DefaultPageSize);
        }

        public static PagedList<T> ToPageList<T>(this IEnumerable<T> allItems, int pageIndex, int pageSize)
        {
            int totalItemCount = allItems.Count();
            int pageCount = (int)Math.Ceiling((double)totalItemCount / pageSize);
            if (pageIndex < DefaultPageIndex)
            {
                pageIndex = DefaultPageIndex;
            }
            else if (pageIndex > pageCount)
            {
                pageIndex = pageCount;
            }

            IEnumerable<T> items = allItems.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return new PagedList<T>(items, pageIndex, pageSize, totalItemCount);
        }
    }
}
