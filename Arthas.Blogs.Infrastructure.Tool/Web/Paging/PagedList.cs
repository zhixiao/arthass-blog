﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.Web.Paging
{
    public class PagedList<T> : IEnumerable<T>
    {
        private readonly IEnumerable<T> _items;
        private readonly int _pageIndex;
        private readonly int _pageSize;
        private readonly int _totalItemCount;

        public PagedList(IEnumerable<T> items, int pageIndex, int pageSize, int totalItemCount)
        {
            _items = items;
            _pageIndex = pageIndex;
            _pageSize = pageSize;
            _totalItemCount = totalItemCount;
        }

        public int PageIndex
        {
            get
            {
                return _pageIndex;
            }
        }

        public int PageSize
        {
            get
            {
                return _pageSize;
            }
        }

        public int TotalItemCount
        {
            get
            {
                return _totalItemCount;
            }
        }

        private int? _pageCount;
        public int PageCount
        {
            get
            {
                if (!_pageCount.HasValue)
                {

                }
                return _pageCount.Value;
            }
        }

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion
    }
}
