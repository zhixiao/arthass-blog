﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.FileStorages
{
    public interface IFileStorage
    {
        Uri Save(Stream stream, string container, string path);

        void Remove(string container, string path);

    }
}
