﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace Arthas.Blogs.Infrastructure.Tool.FileStorages
{
    public class FileStorage : IFileStorage
    {
        private string _defaultContainer = "";
        private string _defaultPath = "";
        public Uri Save(Stream stream, string Container, string path)
        {
            if (string.IsNullOrWhiteSpace(Container)) { Container = _defaultContainer; }
            if (string.IsNullOrWhiteSpace(path)) { path = _defaultPath; }

            Container = Container.ToLower();
            path = path.ToLower();

            if (path.StartsWith("/" + Container)) 
            {
                path = path.Substring(path.IndexOf("/" + Container) + Container.Length + 2);
            }

            var virtualPath = string.Format("~/{0}/{1}", Container, path);
            var physicalPath = HostingEnvironment.MapPath(virtualPath);
            var directory = Path.GetDirectoryName(physicalPath);
            if (!Directory.Exists(directory)) 
            {
                Directory.CreateDirectory(directory);
            }
            using (var fs = File.Open(physicalPath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            {
                if (stream.CanSeek)
                {
                    stream.Seek(0, SeekOrigin.Begin);
                }
                stream.CopyTo(fs);
            }

            return new Uri(virtualPath.Replace("~/", HostingEnvironment.ApplicationVirtualPath == "/" ? HostingEnvironment.ApplicationVirtualPath : HostingEnvironment.ApplicationVirtualPath + "/"), UriKind.Relative);
        }


        public void Remove(string container, string path)
        {
            if (string.IsNullOrWhiteSpace(container)) { container = _defaultContainer; }
            if (string.IsNullOrWhiteSpace(path)) { path = _defaultPath; }

            container = container.ToLower();
            path = path.ToLower();
            var appPath = HostingEnvironment.ApplicationVirtualPath.ToLower();
            if (!path.StartsWith(appPath))
            {
                path = string.Format("~/{0}/{1}", container, path);
            }
            var physicalPath = HostingEnvironment.MapPath(path);
            if (File.Exists(physicalPath))
                File.Delete(physicalPath);
        }
    }
}
