﻿using Arthas.Blogs.Infrastructure.Tool.FileStorages;
using Arthas.Blogs.Infrastructure.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool
{
    public class ToolStartUpTask : IStartupTask
    {
        public void Execute()
        {
            Container.Current.Register<IFileStorage, FileStorage>();
        }

        public int Order
        {
            get { return 0; }
        }
    }
}
