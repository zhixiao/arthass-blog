﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.CQRS
{
    public class Command
    {
        public Command()
        {
            this.ID = Guid.NewGuid();
        }

        public Guid ID { get; private set; }
    }
}
