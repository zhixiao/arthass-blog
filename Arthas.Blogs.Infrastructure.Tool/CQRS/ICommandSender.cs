﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.CQRS
{
    public interface ICommandSender
    {
        void Send<T>(T Command) where T : Command;

        Task SendAsync<T>(T Command) where T : Command;
    }
}
