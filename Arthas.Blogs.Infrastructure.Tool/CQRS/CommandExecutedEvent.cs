﻿namespace Arthas.Blogs.Infrastructure.Tool.CQRS
{
    public class CommandExecutedEvent : IDomainEvent
    {
        public CommandExecutedEvent(Command command)
        {
            Command = command;
        }

        public Command Command { get; private set; }
    }
}
