﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.CQRS
{
    public class ServiceBus : ICommandSender , IEventPublisher
    {
        private readonly Dictionary<Type, List<Delegate>> eventRoutes = new Dictionary<Type, List<Delegate>>();
        private readonly Dictionary<Type, Delegate> commandRoutes = new Dictionary<Type, Delegate>();
        private readonly List<CommandResult> commandResults = new List<CommandResult>();

        static object cmdLocker = new object();
        static object evtLocker = new object();
        static object resultLocker = new object();

        public ServiceBus()
        {

        }

        #region Command

        public void RegisterCommandHandler<T>(Action<T> commandHandler) where T : Command
        {
            Delegate handler;

            lock (cmdLocker)
            {
                if (!commandRoutes.TryGetValue(typeof(T), out handler))
                {
                    commandRoutes.Add(typeof(T), commandHandler);
                }
            }
        }

        public void Send<T>(T command) where T : Command
        {
            if (command == null) throw new ArgumentNullException("command");

            Delegate handler;

            lock (cmdLocker)
            {
                if (commandRoutes.TryGetValue(typeof(T), out handler) == false)
                {
                    throw new InvalidOperationException("No command handler is registerd");
                }
            }

            if (handler is Action<T>)
            {
                ((Action<T>)handler)(command);
                Publish(new CommandExecutedEvent(command));
            }
        }

        public async Task SendAsync<T>(T command) where T : Command
        {
            if (command == null) throw new ArgumentNullException("command");

            Delegate handler;

            lock (cmdLocker)
            {
                if (commandRoutes.TryGetValue(typeof(T), out handler) == false)
                {
                    throw new InvalidOperationException("No command handler is registerd");
                }
            }

            if (handler is Action<T>)
            {
                await Task.Factory.StartNew(() => ((Action<T>)handler)(command));
            }

            await Task.Factory.StartNew(() => { });
        }

        public void Return(CommandResult result)
        {
            if (result == null)
            {
                return;
            }

            lock (resultLocker)
            {
                if (commandResults.Any(item => item.CommandID == result.CommandID))
                {
                    throw new Exception("Result is returned duplicately");
                }

                commandResults.Add(result);
                commandResults.RemoveAll(item => item.ExpiredAt < DateTime.UtcNow);
            }
        }

        public CommandResult GetResult(Guid commandID)
        {
            lock (resultLocker)
            {
                return commandResults.FirstOrDefault(item => item.CommandID == commandID && item.ExpiredAt >= DateTime.UtcNow);
            }
        }

        #endregion

        #region Event

        public void RegisterEventHandler<T>(Action<T> eventHandler) where T : IDomainEvent
        {
            List<Delegate> handlers;
            lock (evtLocker)
            {
                if (!eventRoutes.TryGetValue(typeof(T), out handlers))
                {
                    handlers = new List<Delegate>();
                    eventRoutes.Add(typeof(T), handlers);
                }

                handlers.Add(eventHandler);
            }
        }

        public void Publish<T>(T @event) where T : IDomainEvent
        {
            if (@event == null) throw new ArgumentNullException("@event");

            List<Delegate> handlers;

            lock (evtLocker)
            {
                if (eventRoutes.TryGetValue(typeof(T), out handlers) == false)
                {
                    return;
                }
            }

            handlers.ForEach(action =>
                {
                    if (action is Action<T>)
                    {
                        Task.Factory.StartNew(() => ((Action<T>)action)(@event));
                    }
                });
        }

        public void PublishSync<T>(T @event) where T : IDomainEvent
        {
            if (@event == null) throw new ArgumentNullException("@event");

            List<Delegate> handlers;

            lock (evtLocker)
            {
                if (eventRoutes.TryGetValue(typeof(T), out handlers) == false)
                {
                    return;
                }
            }

            handlers.ForEach(action => 
            {
                if (action is Action<T>)
                {
                    ((Action<T>)action)(@event);
                }
            });
        }

        public void RemoveEventHandler<T>(Action<T> handler)
        {
            List<Delegate> handlers;
            lock (evtLocker)
            {
                if (eventRoutes.TryGetValue(typeof(T), out handlers))
                {
                    handlers = new List<Delegate>();

                }
            }
        }

        #endregion
    }
}
