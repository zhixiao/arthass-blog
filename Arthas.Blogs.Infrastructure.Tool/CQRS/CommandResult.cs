﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.CQRS
{
    public class CommandResult
    {
        protected CommandResult()
        { }

        public CommandResult(Guid commandID,int errorCode = 0,int timeout = 50)
        { 
#if DEBUG
            timeout = 5000;
#endif
            this.CommandID = commandID;
            ErrorCode = errorCode;
            ExpiredAt = DateTime.UtcNow.Add(TimeSpan.FromSeconds(timeout));
        }

        public CommandResult(Command command, int errorCode = 0, int timeout = 50)
            : this(command.ID, errorCode, timeout)
        { 
        }

        public Guid CommandID { get; private set; }

        public int ErrorCode { get; private set; }

        public DateTime ExpiredAt { get; private set; }
    }
}
