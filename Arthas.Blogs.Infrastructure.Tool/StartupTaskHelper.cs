﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool
{
    public class StartupTaskHelper
    {
        public virtual ITypeFinder TypeFinder
        {
            get
            {
                return new AppDomainTypeFinder();
            }
        }

        public virtual void RunStartupTask()
        {
            var startupTaskTypes = TypeFinder.FindClassesOfType<IStartupTask>();

            var tasks = new List<IStartupTask>();
            foreach (var type in startupTaskTypes)
            {
                tasks.Add((IStartupTask)Activator.CreateInstance(type));
            }

            foreach (var task in tasks)
            {
                task.Execute();
            }
        }
    }
}
