﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool
{
    public static class EnumExtension
    {
        static Dictionary<string, string> Descriptions = new Dictionary<string, string>();

        public static string Description(this Enum @enum)
        {
            var enumType = @enum.GetType();
            var key = enumType.ToString() + "_" + @enum.ToString();

            if (!Descriptions.ContainsKey(key))
            {
                FieldInfo info = enumType.GetField(@enum.ToString());

                if (info != null)
                {
                    var attributes = (DescriptionAttribute[])info.GetCustomAttributes(typeof(DescriptionAttribute), false);

                    if (attributes != null && attributes.Length > 0)
                    {
                        Descriptions[key] = attributes[0].Description;

                        return Descriptions[key];
                    }
                }

                Descriptions[key] = @enum.ToString();
            }

            return Descriptions[key];
        }
    }
}
