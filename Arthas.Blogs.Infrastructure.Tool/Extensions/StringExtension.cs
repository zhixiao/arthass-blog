﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool
{
    public static class StringExtension
    {
        public static string Cut(this string str, int leaveNum, string more = "...")
        {
            var rtn = str;
            if (string.IsNullOrEmpty(str) == false)
            { 
                if(str.Length > leaveNum)
                {
                    rtn = str.Substring(0, leaveNum);
                    for (var i = leaveNum - 1; i >= 0; i--)
                    {
                        if (char.IsLetterOrDigit(rtn[i]))
                        {
                            rtn = rtn.Substring(0, i);
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (rtn.Length > 0)
                        rtn = rtn.Remove(rtn.Length - 1);

                    if (rtn.Length == 0)
                        rtn = str.Substring(0, leaveNum);

                    rtn += " " + more;
                }
            }

            return rtn;
        }
    }
}
