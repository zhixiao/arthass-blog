﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool
{
    public interface IStartupTask
    {
        void Execute();

        int Order { get; }
    }
}
