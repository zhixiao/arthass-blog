﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.Exceptions
{
    public class KoobooProjectExceptionBase : Exception
    {
        public KoobooProjectExceptionBase(string exceptionCode, string message)
            : base(message)
        {
            ExceptionCode = exceptionCode;
        }

        public string ExceptionCode
        {
            get;
            private set;
        }
    }
}
