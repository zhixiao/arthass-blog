﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arthas.Blogs.Infrastructure.Tool.Exceptions
{
    public class InfoMessage
    {
        public InfoMessage(bool succ,string errorMessage = "",string stackTrace = "")
        {
            Success = succ;
            ErrorMessage = errorMessage;
            StackTrace = stackTrace;
        }

        public bool Success { get; set; }

        public string ErrorMessage { get; set; }

        public string StackTrace { get; set; }
    }
}
